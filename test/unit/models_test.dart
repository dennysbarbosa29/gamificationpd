// import "package:flutter_test/flutter_test.dart";
// import 'package:gamefication/models/game_model.dart';
// import "package:gamefication/services/game_fetcher.dart";

// void main() {

//   List<Game> _gameListMock = [
//   Game(
//     name: 'GameLabs',
//     ranking: 2,
//     currentRewardPoints: 3993,
//     totalRewardPoints: 5143,
//     mission: Mission(
//       currentProgress: 0.05,
//       desiredProgress: 0.1,
//       goalOfTheDay: 0.2,
//     ),
//   ),
//   Game(
//     name: 'PokerTech',
//     ranking: 4,
//     currentRewardPoints: 455,
//     totalRewardPoints: 2898,
//     mission: Mission(
//       currentProgress: 0.4,
//       desiredProgress: 0.46,
//       goalOfTheDay: 0.57,
//     ),
//   ),
//   Game(
//     name: 'Game3',
//     ranking: 17,
//     currentRewardPoints: 200,
//     totalRewardPoints: 200,
//     mission: Mission(
//       currentProgress: 0.67,
//       desiredProgress: 0.75,
//       goalOfTheDay: 0.9,
//     ),
//   ),
//   Game(
//     name: 'Game44',
//     ranking: 100,
//     currentRewardPoints: 0,
//     totalRewardPoints: 100,
//     mission: Mission(
//       currentProgress: 0.0,
//       desiredProgress: 0.1,
//       goalOfTheDay: 0.25,
//     ),
//   ),
//   Game(
//     name: 'Game555',
//     ranking: 4,
//     currentRewardPoints: 2100,
//     totalRewardPoints: 6200,
//     mission: Mission(
//       currentProgress: 0.9,
//       desiredProgress: 1,
//       goalOfTheDay: 1,
//     ),
//   ),
// ];
//   test(
//     'Test Game Model Creation from Json',
//     () async{
//       IGameFetcher gameFetcher = JsonGameFetcherAdapter();
//       final List<Game> result = await gameFetcher.getGamesList();
//       expect(result, _gameListMock);
//     },
//   );
// }
