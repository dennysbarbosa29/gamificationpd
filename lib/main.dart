import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/ui/controllers/profile_picture_controller.dart';
import 'package:gamification/ui/controllers/versioncheckprovider.dart';
import 'package:gamification/ui/router.dart';
import 'package:gamification/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

import 'package:flutter/cupertino.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        StreamProvider<FirebaseUser>.value(
          value: FirebaseAuth.instance.onAuthStateChanged,
        ),
        ChangeNotifierProvider<VersionCheck>(
          create: (_) => VersionCheck(),
        ),
        ChangeNotifierProvider<HomeScreenController>(
          create: (_) => HomeScreenController(),
        ),
        ChangeNotifierProvider<ProfilePictureController>(
          create: (_) => ProfilePictureController(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Gamefication',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          scaffoldBackgroundColor: AppColors.backgroundLightBlue,
          appBarTheme: AppBarTheme(
            elevation: 0.0,
            color: AppColors.appBarBlue,
            iconTheme: IconThemeData(color: Colors.white,),
            textTheme: TextTheme(
              title: TextStyle(
                fontSize: 22,
                color: Colors.white,
              ),
            ),
          ),
          textTheme: TextTheme(
            title: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w600,
              color: AppColors.defaultGrey,
            ),
            display1: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.w400,
              color: Colors.black87,
            ),
            body1: TextStyle(
              fontSize: 18.5,
              fontWeight: FontWeight.normal,
              color: Colors.black,
            ),
            body2: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              color: Colors.white,
            ),
            headline: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
              color: Colors.orange,
            ),
            subhead: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w300,
              color: Colors.grey.withOpacity(0.95),
            ),
          ),
          iconTheme: IconThemeData(size: 32.0, color: Colors.black),
        ),
        onGenerateRoute: Router.generateRoute,
        initialRoute: splashRoute,
      ),
    );
  }
}
