import 'package:intl/intl.dart';

class Formatters{
  final DateFormat _dateFormatter = DateFormat("dd/MM/yyyy");
  final DateFormat _dateAndTimeFormatter = DateFormat("dd/MM HH:mm");
  final NumberFormat _longIntFormatter = NumberFormat("#,###");

  String date(DateTime date){
    return _dateFormatter.format(date);
  }

  String dateAndTime(DateTime date){
    return _dateAndTimeFormatter.format(date);
  }

  String intWithDot(int number){
    return _longIntFormatter.format(number).replaceAll(',', '.');
  }
}