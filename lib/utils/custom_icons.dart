import 'package:flutter/widgets.dart';

class CustomIcons {
  CustomIcons._();

  static const _fontFamily = 'CustomIcons';

  static const IconData facebook = const IconData(0xf09a, fontFamily: _fontFamily);
  static const IconData google = const IconData(0xf1a0, fontFamily: _fontFamily);
  static const IconData facebook_official = const IconData(0xf230, fontFamily: _fontFamily);
}