import 'package:flutter/material.dart';

class AppIcons {
  static List<String> missionIconOptions = [
    'gamepad',
    'star_border',
    'directions_run',
    'card_giftcard'
  ];

  static IconData getIconDataFromIconName(String iconName) {
    if (iconName == 'gamepad') {
      return Icons.gamepad;
    } else if (iconName == 'star_border') {
      return Icons.star_border;
    } else if (iconName == 'directions_run') {
      return Icons.directions_run;
    } else if (iconName == 'card_giftcard') {
      return Icons.card_giftcard;
    } else {
      return Icons.gamepad; // Default
    }
  }

  static int getIconIndexFromIconName(String iconName) {
    int index = 0;
    for (String name in missionIconOptions) {
      if (name == iconName) return index;
      index++;
    }
    return 0;
  }
}