import 'dart:convert';

import 'package:gamification/models/mission_accomplished_model.dart';
import 'package:gamification/models/purchased_reward_model.dart';
import 'package:gamification/services/historic_service.dart';


class HistoricRepository {
  List<PurchasedReward> rewards;
  List<MissionAccomplished> missions;

  Future<void> getHistoric() async {
    try {
      Map<String, dynamic> json = await HistoricService.getUserHistoric();
      missions = List<MissionAccomplished>();
      rewards = List<PurchasedReward>();
      // Map<String, dynamic> json = await jsonDecode(historicMock);
      if (json['historics']['rewards_historic'] != null) {
        json['historics']['rewards_historic'].forEach((v) {
          rewards.add(new PurchasedReward.fromJson(v));
        });
      }
      if (json['historics']['missions_historic'] != null) {
        json['historics']['missions_historic'].forEach((v) {
          missions.add(new MissionAccomplished.fromJson(v));
        });
      }
    } catch (exception) {
      throw exception;
    }
  }

  String historicMock = """{
  "historics":{
    "missions_historic":[

    ],
    "rewards_historic":[
      {
        "color":"4294198070",
        "subTitle":"Maratonar Vikings",
        "dateLimit":"1583772057228",
        "limit":"20",
        "used":"0",
        "title":"Netflix",
        "value":"50",
        "points":"100"
      },
      {
        "subTitle":"Maratonar Vikings",
        "color":"4294198070",
        "dateLimit":"1583772057228",
        "limit":"20",
        "used":"0",
        "title":"Netflix",
        "value":"50",
        "points":"100"
      }
    ]
  }
}""";
}
