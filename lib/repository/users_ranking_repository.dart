import 'dart:convert';

import 'package:gamification/models/users_ranking_model.dart';
import 'package:gamification/services/users_ranking_service.dart';


class UsersRankingRepository {
  List<RankingUser> users;

  Future<void> getUsers() async {
    try {
      Map<String, dynamic> json = await UsersRankingService.getUsersRanking();
      // Map<String, dynamic> json = await jsonDecode(rankingListMock);
      if (json['bestUserListRanking'] != null) {
        users = List<RankingUser>();
        json['bestUserListRanking'].forEach((v) {
          users.add(new RankingUser.fromJson(v));
        });
      }
    } catch(exception) {
      throw exception;
    }
  }

  String rankingListMock = """{
    "bestUserListRanking": [
      {
        "image_path": "",
        "name": "Luiz",
        "registration": "3819008",
        "email": "luiz-a-tavares@openlabs.com.br",
        "points": 6000
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "https://drive.google.com/uc?id=1mxqzL6WN6D8QARg0epFcv3pVcCt-A8tr",
        "name": "Diogo",
        "registration": "3819007",
        "email": "diogo-v-oliveira@openlabs.com.br",
        "points": 1500
      },
      {
        "image_path": "",
        "name": "Yuri",
        "registration": "3819009",
        "email": "yuri-m-moreira@openlabs.com.br",
        "points": 300
      },
      {
        "image_path": "",
        "name": "Dennys",
        "registration": "3819001",
        "email": "dennys-b-silva@openlabs.com.br",
        "points": 10
      }
    ]
  }""";

}