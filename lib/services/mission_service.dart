import 'package:flutter/material.dart';
import 'package:gamification/utils/urls.dart';

import 'package:http/http.dart' as http;

class MissionServiceResult {
  bool status;
  String errorCode;
  String errorMessage;
}

class MissionService {
  static String urlEndPoint = AppUrls.endPoint;
  static String failedOperationCode = 'ERROR_FAILED_TO_SET_MISSION_ACTIVE';
  static String failedOperationMessage = 'Failed to associate user to mission. Code: ';

  static Future<MissionServiceResult> setUserParticipatingInMission({@required String uid, @required String missionId}) async {
    MissionServiceResult missionResult = MissionServiceResult();

    String url = urlEndPoint + "/mission_active";
    String body = '''
    {
      "userID": "$uid",
      "missionID": "$missionId",
      "dateCreate": "${DateTime.now().millisecondsSinceEpoch}",
      "finish": false
    }
     ''';

    Map<String, String> headers = new Map<String, String>();
    headers["Content-type"] = "application/json";
    
    try {
      http.Response response = await http.post(url, body: body, headers: headers);
      if (response.statusCode == 200) {
        missionResult.status = true;
      } else {
        missionResult.status = false;
        missionResult.errorCode = failedOperationCode;
        missionResult.errorMessage = failedOperationMessage + response.statusCode.toString();
      }
      return missionResult;

    } catch (exception) {
      missionResult.status = false;
      missionResult.errorCode = failedOperationCode;
      missionResult.errorMessage = failedOperationMessage + exception.toString();
      return missionResult;
    }
  }
}