import 'dart:convert';

import 'package:gamification/models/user_model.dart';
import 'package:gamification/utils/urls.dart';
import 'package:http/http.dart' as http;

class HistoricService {
  static Future<Map<String, dynamic>> getUserHistoric() async {
    String url = AppUrls.endPoint + '/historic?userID=${User.uid}';

    Map<String, String> headers = Map<String, String>();
    headers["Content-type"] = "application/json";

    Map<String, dynamic> parsedJson;

    try {
      http.Response response =
          await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        parsedJson = json.decode(response.body);
        return parsedJson;
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Code ${response.statusCode}');
      }
    } catch (exception) {
      throw exception;
    }
  }
}