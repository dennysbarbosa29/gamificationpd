import 'package:flutter/services.dart';
import 'package:gamification/models/game_model.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/utils/urls.dart';
import 'dart:convert';
import 'package:http/http.dart';

abstract class IGameFetcher {
  Future<String> _fetchData();
  Future<List<Game>> getGamesList();
  //Game getSingleGame(String gameName);
  bool shouldUpdateData();
}

class JsonGameFetcherAdapter implements IGameFetcher {
  //static const int _numberOfGames = 5;

  @override
  Future<String> _fetchData() async {
    //TODO: change this to dart file lib or create a widget test???
    return await rootBundle.loadString('assets/jsons/games3.json');
  }

  @override
  Future<List<Game>> getGamesList() async {
    List<Game> _gameList = [];
    Map<String, dynamic> _parsedJson = json.decode(await _fetchData());
    for (Map<String, dynamic> gameJson in _parsedJson['games']) {
      _gameList.add(Game.fromJson(gameJson['game']));
    }
    return _gameList;
  }

  @override
  bool shouldUpdateData() {
    return false;
  }
}

class NetworkGameFetcherAdapter implements IGameFetcher {
  List<Game> _gameList = [];
  String _urlEndPoint = AppUrls.endPoint;
  String _uid = '';
  bool _reload = false;

  String get uid => _uid;

  void setUid(String uid) {
    _uid = uid;
  }

  set reload(bool flag) => _reload = flag;

  @override
  Future<String> _fetchData() async {
    String url = _urlEndPoint + '/game/phone?UID=$_uid';

    Map<String, String> headers = Map<String, String>();
    headers["Content-type"] = "application/json";

    try {
      Response response = await get(url, headers: headers);

      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception('Failed to load games. Code ${response.statusCode}');
      }
    } catch (exception) {
      throw exception;
    }
    // Descomentar essas duas linhas abaixo e comentar o try catch para testar mockado caso o servidor pare de responder
    // String response = await rootBundle.loadString('assets/jsons/games3.json');
    // return response;
  }

  @override
  Future<List<Game>> getGamesList() async {
    if (_gameList.length > 0 && !_reload) {
      return _gameList;
    } else {
      try {
        if (_reload) _gameList = [];
        Map<String, dynamic> parsedJson = json.decode(await _fetchData());
        for (Map<String, dynamic> gameJson in parsedJson['games']) {
          _gameList.add(Game.fromJson(gameJson['game']));
        }
        User.displayName = parsedJson['user']['name'];
        User.registration = parsedJson['user']['registration'];
        User.email = parsedJson['user']['email'];
        User.points = parsedJson['user']['points'];
        User.ranking = parsedJson['user']['ranking'];
        User.active = parsedJson['user']['active'];
        if (parsedJson['user']['totalPoints'] == null) {
          User.totalPoints = 0;
        } else {
          User.totalPoints = parsedJson['user']['totalPoints'];
        }

        User.message = parsedJson['user']['message'];
        User.performance = parsedJson['user']['performance'];
        if (!(User.active)) {
          throw ('Acesso negado! Entre em contato com o administrador para mais informações.');
        }
      } catch (exception) {
        throw exception;
      }
      return _gameList;
    }
  }

  @override
  bool shouldUpdateData() {
    return false;
  }
}
