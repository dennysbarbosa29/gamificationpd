import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:gamification/utils/urls.dart';
import 'package:http/http.dart' as http;

class ProfilePictureService {
  static FirebaseStorage _firebaseStorage = FirebaseStorage();
  static String urlEndPoint = AppUrls.endPoint;

  static Future<String> getProfilePictureURL(String uid) async {
    StorageReference imageReference = _firebaseStorage.ref().child('user/$uid/profile_picture.png');
    try {
      String imageURL = await imageReference.getDownloadURL();
      return imageURL;
    } on PlatformException catch (e) {
      throw e;
    }
  }

  static Future<File> downloadProfilePictureToPath(String uid, String path) async {
    StorageReference imageReference = _firebaseStorage.ref().child('user/$uid/profile_picture.png');
    try {
      final File imageFile = File(path);
      if (imageFile.existsSync()) {
        await imageFile.delete();
      }
      await imageFile.create();
      assert(await imageFile.readAsString() == "");
      final StorageFileDownloadTask downloadTask = imageReference.writeToFile(imageFile);
      await downloadTask.future;
      return imageFile;
    } on Exception catch (e) {
      throw e;
    }
  }

  static Future<void> uploadProfilePicture(String uid, File file) async {
    StorageReference imageReference = _firebaseStorage.ref().child('user/$uid/profile_picture.png');
    if (file == null) return;
    try {
      StorageUploadTask imageUploadTask = imageReference.putFile(file);

      while(!imageUploadTask.isComplete) {
        await Future.delayed(Duration(milliseconds: 100));
      }
    } on PlatformException catch(e) {
      throw e;
    }
  }

  static Future<void> saveProfilePictureUrl(String uid, String imageUrl) async {
    String url = urlEndPoint + "/user/phone/?userID=" + uid;
    Map<String, String> headers = new Map<String, String>();
    headers["Content-type"] = "application/json";

    String body = '''
    {
      "path": "${imageUrl.split('&')[0]}"
    }
     ''';

    try {
      http.Response response = await http.put(url, headers: headers, body: body);
      if (response.statusCode == 200 && response.body == 'true') {
        return;
      } else {
        throw Exception('Code: ${response.statusCode}. Body: ${response.body}');
      }
    } catch (exception) {
      throw exception;
    }
  }
}