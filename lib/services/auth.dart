
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/services/firebase_database.dart';
import 'package:gamification/utils/urls.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

import '../main.dart';

class AuthenticationResult {
  bool status;
  String errorCode;
  String errorMessage;
}

class AuthenticationServices {
  static String urlEndPoint = AppUrls.endPoint;

  static String emailNotVerifiedCode = 'ERROR_EMAIL_NOT_VERIFIED';
  static String emailNotVerifiedMessage =
      'Verifique o código enviado para o seu email';
  // static String userAlreadyRegisteredCode = 'ERROR_USER_ALREADY_REGISTERED';
  // static String userAlreadyRegisteredMessage = 'Usuário já cadastrado';
  static String userAlreadyRegisteredCode = 'ERROR_USER_ALREADY_REGISTERED';
  static String userAlreadyRegisteredMessage =
      'Data conflict when creating user';
  static String failedToCreateUserCode = 'ERROR_FAILED_TO_CREATE_USER';
  static String failedToCreateUserMessage = 'Falha ao criar usuário. Codigo: ';

  static FirebaseUser _userToResendEmail;

  static Future<DocumentSnapshot> isUserAuthenticated() async {
    DocumentSnapshot document;

    FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();

    if (firebaseUser == null) {
      return null;
    } else {
      User.uid = firebaseUser.uid;
      print(User.uid);
      // TODO: fix exception in this line
      //document = await FirestoreHandler.getUser(departamento: User.departamento, empresa: User.empresa);
    }

    return document;
  }

  static Future<AuthenticationResult> createUser(
      {@required String name,
      @required String registration,
      @required String email,
      @required String password}) async {
    AuthenticationResult authResult = AuthenticationResult();
    String url = urlEndPoint + "/user";
    bool active = true;
    int points = 0;
    String body = '''
    {
      "name": "$name",
      "registration": "$registration",
      "email": "$email",
      "password": "$password",
      "active":  $active,
      "points":  $points

    }
     ''';
    Map<String, String> headers = new Map<String, String>();
    headers["Content-type"] = "application/json";
    try {
      http.Response response =
          await http.post(url, body: body, headers: headers);
      if (response.statusCode == 200 || response.statusCode == 201) {
        authResult.status = true;

        authResult.errorMessage = response.body;
        authResult.errorCode = response.statusCode.toString();
        print(response.body);
      } else if (response.statusCode == 409) {
        authResult.status = false;
        authResult.errorMessage = response.body;
        authResult.errorCode = response.statusCode.toString();
      } else {
        authResult.status = false;
        authResult.errorMessage = response.body;
        authResult.errorCode = response.statusCode.toString();
      }
      await FirebaseAuth.instance.signOut();
      return authResult;
    } catch (exception) {
      authResult.status = false;
      authResult.errorCode = failedToCreateUserCode;
      authResult.errorMessage =
          failedToCreateUserMessage + exception.toString();
          await FirebaseAuth.instance.signOut();
      return authResult;
    }
  }

  ///Atualiza as informações do usuário [displayName]
  static void _updateUserInfo(FirebaseUser user) async {
    UserUpdateInfo updateInfo = UserUpdateInfo();
    updateInfo.displayName = User.displayName;
    await user.updateProfile(updateInfo);
  }

  static Future<AuthenticationResult> emailSignUp(String password) async {
    AuthenticationResult authResult = AuthenticationResult();

    try {
      FirebaseUser user =
          (await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: User.email,
        password: password,
      ))
              .user;

      //Salva o uid gerado
      User.uid = user.uid;

      _updateUserInfo(user);

      //Adiciona o usuário no banco de dados
      FirestoreHandler.addUser(
          empresa: User.empresa, departamento: User.departamento);

      await user.sendEmailVerification();

      authResult.status = true;
      return authResult;
    } on PlatformException catch (error) {
      authResult.errorCode = error.code;
      authResult.errorMessage = error.message;
      authResult.status = false;
      return authResult;
    }
  }

  static Future<AuthenticationResult> emailSingIn(String password) async {
    AuthenticationResult authResult = AuthenticationResult();

    try {
      FirebaseUser user =
          (await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: User.email,
        password: password,
      ))
              .user;

      User.uid = user.uid;
      User.displayName = user.displayName;

      if (user.isEmailVerified) {
        authResult.status = true;
      } else {
        _userToResendEmail = user;
        authResult.status = false;
        authResult.errorCode = emailNotVerifiedCode;
        authResult.errorMessage = emailNotVerifiedMessage;
        await FirebaseAuth.instance.signOut();
      }

      return authResult;
    } on PlatformException catch (error) {
      authResult.errorCode = error.code;
      authResult.errorMessage = error.message;
      authResult.status = false;
      return authResult;
    }
  }

  static Future<void> userLogout() async {
    await FirebaseAuth.instance.signOut();

    // await FacebookLogin().logOut();

    // try {
    //   await GoogleSignIn().disconnect();
    // } catch (e) {}

    main();
  }

  static Future<void> resendVerificationEmail() async {
    try {
      await _userToResendEmail.sendEmailVerification();
    } catch (exception) {
      return exception;
    }
  }

  static Future<AuthenticationResult> resetPassword({@required String email}) async {
    AuthenticationResult authResult = AuthenticationResult();
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      authResult.status = true;
      return authResult;
    } catch (e) {
      authResult.status = false;
      authResult.errorCode = e.code;      
      authResult.errorMessage = e.toString();
      return authResult;
    }
  }
}
