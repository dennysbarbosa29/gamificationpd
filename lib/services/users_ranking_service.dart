import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/utils/urls.dart';
import 'package:http/http.dart' as http;

class UsersRankingService {
  static Future<Map<String, dynamic>> getUsersRanking() async {
    String url = AppUrls.endPoint + '/game/phone?UID=${User.uid}';

    Map<String, String> headers = Map<String, String>();
    headers["Content-type"] = "application/json";

    Map<String, dynamic> parsedJson;

    try {
      http.Response response = await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        parsedJson = json.decode(response.body);
        return parsedJson;
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Code ${response.statusCode}');
      }
    } catch (exception) {
      throw exception;
    }
  }

  static Future<bool> updateUserMessage({
    @required String userMessage,
  }) async {
    String url = AppUrls.endPoint + '/user/phone/message?userID=${User.uid}';
    String body = '''
    {
      "message": "$userMessage"
    }
     ''';
    Map<String, String> headers = Map<String, String>();
    headers["Content-type"] = "application/json";

     try {
      http.Response response =
          await http.put(url, body: body, headers: headers);
      print(response.statusCode);
      if (response.statusCode == 201 || response.statusCode == 200) {
        return true; // Success
      } else {
        throw Exception(
            'Failed to update user points by mission. Code ${response.statusCode}');
      }
    } catch (exception) {
      throw exception;
    }
  }
}
