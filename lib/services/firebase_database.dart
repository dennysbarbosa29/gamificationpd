import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gamification/models/user_model.dart';


class FirestoreHandler {

///Retorna documento [QuerySnapshot] do usuário selecionado
static Future<DocumentSnapshot> getUser({String departamento, String empresa}) async{
  DocumentSnapshot document = await Firestore.instance.collection('$empresa/departamento/$departamento')
  .document(User.uid).get();

  return document;
}

static Future<DocumentSnapshot> getUserEmail({String departamento, String empresa}) async{
  DocumentSnapshot document = await Firestore.instance.collection('$empresa/departamento/$departamento')
  .document(User.email).get();

  return document;
}

///Adiciona usuário ao banco de dados
static void addUser({String departamento, String empresa}) async{
  await Firestore.instance.collection('$empresa/departamento/$departamento').document(User.uid)
  .setData({ 'uid': User.uid , 'displayName': User.displayName ,
             'email': User.email, 'registration': User.registration});
}

}