import 'package:flutter/material.dart';
import 'package:gamification/utils/urls.dart';
import 'package:http/http.dart' as http;

class RewardServiceResult {
  bool status;
  String errorCode;
  String errorMessage;
}

class RewardService {
  static String urlEndPoint = AppUrls.endPoint;
  static String failedOperationCode = 'ERROR_FAILED_TO_REQUEST_REWARD';
  static String failedOperationMessage = 'Failed to request reward. Code: ';

  static Future<RewardServiceResult> requestReward({@required String uid, @required String rewardId}) async {
    RewardServiceResult rewardResult = RewardServiceResult();

    String url = urlEndPoint + "/reward_active";
    String body = '''
    {
      "userID": "$uid",
      "rewardID": "$rewardId",
      "dateCreate": "${DateTime.now().millisecondsSinceEpoch}",
      "finish": false
    }
     ''';

    Map<String, String> headers = new Map<String, String>();
    headers["Content-type"] = "application/json";

    try {
      http.Response response = await http.post(url, body: body, headers: headers);
      if (response.statusCode == 200) {
        rewardResult.status = true;
      } else {
        rewardResult.status = false;
        rewardResult.errorCode = failedOperationCode;
        rewardResult.errorMessage = failedOperationMessage + response.statusCode.toString();
      }
      return rewardResult;

    } catch (exception) {
      rewardResult.status = false;
      rewardResult.errorCode = failedOperationCode;
      rewardResult.errorMessage = failedOperationMessage + exception.toString();
      return rewardResult;
    }
  }
}