class RankingUser {
  String name;
  String email;
  String photoUrl;
  String registration; // Matricula do cara na empresa
  int points;
  String message;

  RankingUser.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    setPhotoUrl(json['image_path']);
    registration = json['registration'];
    if (json['totalPoints'] == null) {
      points = 0;
    } else {
      points = json['totalPoints'];
    }
    setMessage(json['message']);
  }

  void setPhotoUrl(String newPhotoUrl) {
    if (newPhotoUrl != "" && newPhotoUrl != null)
      this.photoUrl = newPhotoUrl;
    else
      this.photoUrl = "";
  }

  void setMessage(String newMessage) {
    if (newMessage != "" && newMessage != null)
      this.message = newMessage;
    else
      this.message = "";
  }
}
