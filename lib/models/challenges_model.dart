import 'package:meta/meta.dart';

class Challenge {
  Challenge({
    @required this.name,
    @required this.description,
    @required this.price,
    @required this.challengeGoal,
    @required this.challengeProgress,
  })  : assert(name != null && name.isNotEmpty),
        assert(description != null && description.isNotEmpty),
        assert(price != null && price.isFinite && !price.isNegative),
        assert(challengeGoal != null &&
            challengeGoal.isFinite &&
            !challengeGoal.isNegative),
        assert(challengeProgress != null &&
            challengeProgress.isFinite &&
            !challengeProgress.isNegative &&
            challengeProgress <= challengeGoal);

  factory Challenge.fromJson(Map<String, dynamic> json) {
    return Challenge(
      name: json['name'],
      description: json['description'],
      price: int.parse(json['price']),
      challengeGoal: int.parse(json['challengeGoal']),
      challengeProgress: int.parse(json['challengeProgress']),
    );//cascade operator
  }

  final String name;
  final String description;
  final int price;
  final int challengeGoal;
  int challengeProgress;



  @override
  String toString() {
    return 'Reward : $name | Price : $price | Redemptions : $challengeProgress/$challengeGoal';
  }
}
