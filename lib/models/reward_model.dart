class Reward {
  String subTitle;
  String limit;
  String id;
  String title;
  String points;
  DateTime dateLimit;
  String value;
  String used;
  int color;

  Reward(
      {this.subTitle,
      this.limit,
      this.id,
      this.title,
      this.points,
      this.dateLimit,
      this.value,
      this.used});

  void setSubTitle(String newSubTitle) {
    if (newSubTitle == null) {
      throw FormatException('Null sub title');
    }
    if (newSubTitle == '') {
      throw FormatException('Empty sub title');
    }
    subTitle = newSubTitle;
  }

  void setLimit(String newLimit) {
    if (newLimit == null) {
      throw FormatException('Null limit');
    }
    if (newLimit == '') {
      throw FormatException('Empty limit');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newLimit))) {
      throw FormatException('Reward limit must be numeric');
    }
    limit = newLimit;
  }

  void setId(String newId) {
    if (newId == null) {
      throw FormatException('Null id');
    }
    if (newId == '') {
      throw FormatException('Empty id');
    }
    id = newId;
  }

  void setTitle(String newTitle) {
    if (newTitle == null) {
      throw FormatException('Null title');
    }
    if (newTitle == '') {
      throw FormatException('Empty title');
    }
    title = newTitle;
  }

  void setPoints(String newPoints) {
    if (newPoints == null) {
      throw FormatException('Null points');
    }
    if (newPoints == '') {
      throw FormatException('Empty points');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newPoints))) {
      throw FormatException('Points must be numeric');
    }
    points = newPoints;
  }

  void setDateLimit(String newDateLimit) {
    if (newDateLimit == null) {
      throw FormatException('Null date');
    }
    if (newDateLimit == '') {
      throw FormatException('Empty date');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newDateLimit))) {
      throw FormatException('Date timestamp must be numeric');
    }
    dateLimit = DateTime.fromMillisecondsSinceEpoch(int.parse(newDateLimit));
  }

  void setValue(String newValue) {
    if (newValue == null) {
      throw FormatException('''Null 'value' field''');
    }
    if (newValue == '') {
      throw FormatException('''Empty 'value' field''');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newValue))) {
      throw FormatException(''''Value' field must be numeric''');
    }
    value = newValue;
  }

  void setUsed(String newUsed) {
    if (newUsed == null) {
      throw FormatException('''Null 'used' field''');
    }
    if (newUsed == '') {
      throw FormatException('''Empty 'used' field''');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newUsed))) {
      throw FormatException(''''Used' field must be numeric''');
    }
    used = newUsed;
  }

  void setColor(String newColor) {
    if (newColor == null) {
      throw FormatException('Null color');
    }
    if (newColor == '') {
      throw FormatException('Empty color');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newColor))) {
      throw FormatException('Reward color must be numeric');
    }
    color = int.parse(newColor);
  }

  void incrementUsedTimes() {
    used = (int.parse(used) + 1).toString();
  }

  Reward.fromJson(Map<String, dynamic> json) {
    setSubTitle(json['subTitle']);
    setLimit(json['limit']);
    setId(json['id']);
    setTitle(json['title']);
    setPoints(json['points']);
    setDateLimit(json['dateLimit']);
    setValue(json['value']);
    setUsed(json['used']);
    setColor(json['color']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subTitle'] = this.subTitle;
    data['limit'] = this.limit;
    data['id'] = this.id;
    data['title'] = this.title;
    data['points'] = this.points;
    data['dateLimit'] = this.dateLimit.millisecondsSinceEpoch.toString();
    data['value'] = this.value;
    data['color'] = this.color.toString();

    return data;
  }
}

// Legacy Reward Model
// import 'package:meta/meta.dart';

// enum RewardStatus { canBuy, notEnoughtPoints, maxLimitReached }

// class Reward {
//   Reward({
//     @required this.name,
//     @required this.description,
//     @required this.price,
//     @required this.maxRedemptions,
//     @required this.currentRedemptions,
//   })  : assert(name != null && name.isNotEmpty),
//         assert(description != null && description.isNotEmpty),
//         assert(price != null && price.isFinite && !price.isNegative),
//         assert(maxRedemptions != null &&
//             maxRedemptions.isFinite &&
//             !maxRedemptions.isNegative),
//         assert(currentRedemptions != null &&
//             currentRedemptions.isFinite &&
//             !currentRedemptions.isNegative &&
//             currentRedemptions <= maxRedemptions);

//   factory Reward.fromJson(Map<String, dynamic> json) {
//     return Reward(
//       name: json['name'],
//       description: json['description'],
//       price: int.parse(json['price']),
//       maxRedemptions: int.parse(json['maxRedemptions']),
//       currentRedemptions: int.parse(json['currentRedemptions']),
//     ); //cascade operator
//   }

//   final String name;
//   final String description;
//   final int price;
//   final int maxRedemptions;
//   int currentRedemptions;
//   RewardStatus _rewardStatus;

//   RewardStatus get rewardStatus => _rewardStatus;

//   void setRewardStatus(int availablePoints) {
//     if (this.maxRedemptions == this.currentRedemptions)
//       _rewardStatus = RewardStatus.maxLimitReached;
//     else if (availablePoints >= this.price)
//       _rewardStatus = RewardStatus.canBuy;
//     else
//       _rewardStatus = RewardStatus.notEnoughtPoints;
//   }

//   @override
//   String toString() {
//     return 'Reward : $name | Price : $price | Redemptions : $currentRedemptions/$maxRedemptions | Status : $_rewardStatus';
//   }
// }
