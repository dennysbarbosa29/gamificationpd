
import 'package:gamification/models/challenges_model.dart';
import 'package:gamification/models/mission_model.dart';
import 'package:gamification/models/purchase_history_model.dart';
import 'package:gamification/models/reward_model.dart';
import 'package:meta/meta.dart';

class Game {
  Game({
    @required this.name,
    this.imagePath = 'assets/images/open_labs_black.png', //Implement this
    this.ranking = 1,
    this.currentRewardPoints = 0,
    this.totalRewardPoints = 0,
    @required this.missions,
    @required this.startDate,
    @required this.endDate,
    @required this.rewardsMap,
    @required this.id,

    /// Map<String rewardName, Reward reward>
    this.purchaseHistory,

    /// Map<DateTime date, PurchaseHistory purchase>
    this.challenges,

    /// Map<String(?) challengeType, Challenge challenge>
  })  : assert(name != null && name.isNotEmpty),
        assert(ranking != null && ranking.isFinite && !ranking.isNegative),
        assert(currentRewardPoints != null &&
            currentRewardPoints.isFinite &&
            !currentRewardPoints.isNegative),
        assert(totalRewardPoints != null &&
            totalRewardPoints.isFinite &&
            !totalRewardPoints.isNegative),
        assert(missions != null),
        assert(startDate != null),
        assert(endDate != null) {
    // Contrstuctor Body below
    // if (rewardsMap != null && rewardsMap.isNotEmpty) _initRewardsStatuses();
    // _sortPurchaseHistory();
  }

  factory Game.fromJson(Map<String, dynamic> json) {
    Map<String, Reward> _rewardsMapFromJson = {};
    Map<DateTime, PurchaseHistory> _purchaseHistoryFromJson = {};
    Map<String, Challenge> _challengesFromJson = {};
    List<Mission> _missions = [];

    // Populate Rewards Map
    if (json['rewards'] != null) {
      for (Map<String, dynamic> reward in json['rewards']) {
        _rewardsMapFromJson[reward['reward']['title']] = Reward.fromJson(reward['reward']);
      }
    }

    // Populate mission list
    if (json['missions'] != null) {
      for (Map<String, dynamic> mission in json['missions']) {
        mission['game_id'] = json['id'];
        _missions.add(Mission.fromJson(mission['mission']));
      }
    }

    // //Populate Purchase history Map
    // if (json['purchaseHistory'] != null)
    // for (Map<String, dynamic> purchase in json['purchaseHistory'])
    //   _purchaseHistoryFromJson[DateTime.parse(purchase['date'])] =
    //       PurchaseHistory.fromJson(purchase);

    // //Populate Challenges Map
    // if (json['challenges'] != null)
    // for (Map<String, dynamic> challenge in json['challenges'])
    //   _challengesFromJson[challenge['price']] = Challenge.fromJson(challenge); //maybe change this key.(add challengeType to JSON?)

    return Game(
      name: json['name'],
      //ranking: int.parse(json['ranking']),
      //imagePath: json['imagePath'],
      //currentRewardPoints: int.parse(json['currentRewardPoints']),
      //totalRewardPoints: int.parse(json['totalRewardPoints']),
      missions: _missions,
      startDate: DateTime.parse(json['initialDate']),
      endDate: DateTime.parse(json['endDate']),
      rewardsMap: _rewardsMapFromJson,
      //purchaseHistory: _purchaseHistoryFromJson,
      //challenges: _challengesFromJson,
      id: json['id'],
    );
  }

  final String name;
  final String imagePath;
  DateTime startDate;
  DateTime endDate;
  int ranking;
  int currentRewardPoints;
  int totalRewardPoints;
  List<Mission> missions;
  Map<String, Reward> rewardsMap;
  Map<DateTime, PurchaseHistory> purchaseHistory;
  Map<String, Challenge> challenges;
  String id;

  // void _initRewardsStatuses() {
  //   this.rewardsMap.forEach((key, value) {
  //     value.setRewardStatus(this.currentRewardPoints);
  //   });
  // }

  // sort purchase history map based on map key (date), from newest to oldest date
  void _sortPurchaseHistory() {
    Map<DateTime, PurchaseHistory> sortedMap = Map.fromEntries(
        purchaseHistory.entries.toList()
          ..sort((e1, e2) => e1.key.compareTo(e2.key)));
    purchaseHistory = sortedMap;
  }

  // void updateRewardStatus(String rewardKey) {
  //   this.rewardsMap[rewardKey].setRewardStatus(this.currentRewardPoints);
  // }

  /// returns the purchase history as a `List[String date, String rewardName]`
  ///
  /// if `ascending = true` returns a list from newest date to oldest date
  ///
  /// if `ascending = false` returns a list from oldest to newest date
  // List<List<String>> getSortedPurchaseList([bool ascending = true]) {
  //   List<List<String>> _sortedList = [];
  //   List<DateTime> datesList = this.purchaseHistory.keys.toList()..sort();
  //   if (ascending = false) datesList = datesList.reversed;
  //   for (DateTime date in datesList) {
  //     _sortedList.add([Formatters().date(date), purchaseHistory[date]]);
  //     print(_sortedList);
  //   }
  //   return _sortedList;
  // }

  @override
  String toString() {
    return 'Game $name \nRanking: $ranking\nRewardPoints / Total:\n$currentRewardPoints / $totalRewardPoints';
  }
}

// Legacy Mission Model
// class Mission {
//   Mission({
//     @required this.currentProgress,
//     @required this.desiredProgress,
//     @required this.goalOfTheDay,
//   })  : assert(currentProgress != null &&
//             currentProgress.isFinite &&
//             !currentProgress.isNegative),
//         assert(desiredProgress != null &&
//             desiredProgress.isFinite &&
//             !desiredProgress.isNegative),
//         assert(goalOfTheDay != null &&
//             goalOfTheDay.isFinite &&
//             !goalOfTheDay.isNegative);

//   factory Mission.fromJson(Map<String, dynamic> json) {
//     return Mission(
//       currentProgress: double.parse(json['currentProgress']),
//       desiredProgress: double.parse(json['desiredProgress']),
//       goalOfTheDay: double.parse(json['goalOfTheDay']),
//     );
//   }

//   double currentProgress;
//   double desiredProgress;
//   double goalOfTheDay;

//   void update() {
//     //GET THE NEW PROGRESSES;
//   }

//   @override
//   String toString() {
//     return 'Current Mission :\nCurrent Progress: $currentProgress\nDesired Progress: $desiredProgress\nGoal of the Day: \n$goalOfTheDay';
//   }
// }
