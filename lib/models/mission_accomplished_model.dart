class MissionAccomplished {
  String subTitle;
  String title;
  String points;
  String iconName;
  DateTime date;

  MissionAccomplished({
    this.subTitle,
    this.title,
    this.points,
    this.date,
    this.iconName,
  });

  void setSubTitle(String newSubTitle) {
    if (newSubTitle == null || newSubTitle == '') {
      subTitle = '';
    } else
      subTitle = newSubTitle;
  }

  void setDate(String newDate) {
    if (newDate == null || newDate == '') {
      date = DateTime.fromMillisecondsSinceEpoch(0);
    } else
      date = DateTime.fromMillisecondsSinceEpoch(int.parse(newDate));
  }

  void setTitle(String newTitle) {
    if (newTitle == null || newTitle == '') {
      title = '';
    } else
      title = newTitle;
  }

  void setPoints(String newPoints) {
    if (newPoints == null || newPoints == '') {
      points = '99999999';
    } else if (!(RegExp('^[0-9]*\$').hasMatch(newPoints))) {
      throw FormatException('Points must be numeric');
    } else
      points = newPoints;
  }

  MissionAccomplished.fromJson(Map<String, dynamic> json) {
    setSubTitle(json['description']);
    setTitle(json['name']);
    setPoints(json['points'].toString());
    setDate(json['finishDate']);
    iconName = json['iconName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.subTitle;
    data['name'] = this.title;
    data['points'] = int.parse(this.points);
    data['finishDate'] = this.date.millisecondsSinceEpoch.toString();
    data['iconName'] = this.iconName;
    return data;
  }
}
