class Mission {
  String name;
  String id;
  String gameId;
  String description;
  String reward; // The points reward for completing the mission
  DateTime endDate;
  String iconName;
  int color;
  bool userIsParticipating; // Indicates that the CURRENT user is participating in the mission
  String instructions; // Instructions on how to complete the mission

  void setName(String newName) {
    if (newName == null) {
      throw FormatException('Null mission name');
    }
    if (newName == '') {
      throw FormatException('Empty mission name');
    }
    name = newName;
  }

  void setId(String newId) {
    if (newId == null) {
      throw FormatException('Null mission id');
    }
    if (newId == '') {
      throw FormatException('Empty mission id');
    }
    id = newId;
  }

  void setGameId(String newGameId) {
    gameId = newGameId;
  }

  void setDescription(String newDescription) {
    if (newDescription == null) {
      throw FormatException('Null mission description');
    }
    if (newDescription == '') {
      throw FormatException('Empty mission description');
    }
    description = newDescription;
  }

  void setReward(String newReward) {
    if (newReward == null) {
      throw FormatException('Null mission reward');
    }
    if (newReward == '') {
      throw FormatException('Empty mission reward');
    }
    reward = newReward;
  }

  void setEndDate(String timestamp) {
    if (timestamp == null) {
      throw FormatException('Null mission end date');
    }
    if (timestamp == '') {
      throw FormatException('Empty mission end date');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(timestamp))) {
      throw FormatException('Mission timestamp must be numeric');
    }
    int timestampInt = int.parse(timestamp);
    endDate = DateTime.fromMillisecondsSinceEpoch(timestampInt);
  }

  void setIconName(String newIconName) {
    if (newIconName == null) {
      throw FormatException('Null mission icon name');
    }
    if (newIconName == '') {
      throw FormatException('Empty mission icon name');
    }
    iconName = newIconName;
  }

  void setColor(String newColor) {
    if (newColor == null) {
      throw FormatException('Null mission icon color');
    }
    if (newColor == '') {
      throw FormatException('Empty mission icon color');
    }
    if (!(RegExp('^[0-9]*\$').hasMatch(newColor))) {
      throw FormatException('Mission icon color must be numeric');
    }
    color = int.parse(newColor);
  }

  void setUserIsParticipating(bool newUserIsParticipating) {
    if (newUserIsParticipating == null) {
      throw FormatException('Null mission_active field');
    }
    userIsParticipating = newUserIsParticipating;
  }

  void setInstructions(String newInstructions) {
    if (newInstructions == null) {
      throw FormatException('Null mission instructions field');
    }
    instructions = newInstructions;
  }

  Mission({this.name, this.id, this.gameId, this.description, this.reward});

  Mission.fromJson(Map<String, dynamic> json) {
    setName(json['name']);
    setId(json['id']);
    setGameId(json['game_id']);
    setDescription(json['description']);
    setReward(json['points'].toString());
    setEndDate(json['endDate']);
    setIconName(json['iconName']);
    setColor(json['color']);
    setUserIsParticipating(json['mission_active']);
    setInstructions(json['details']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['id'] = this.id;
    data['game_id'] = this.gameId;
    data['description'] = this.description;
    data['reward'] = this.reward;
    data['endDate'] = this.endDate.millisecondsSinceEpoch.toString();
    data['iconName'] = this.iconName;
    data['color'] = this.color.toString();

    return data;
  }
}

