class PurchasedReward {
  String subTitle;
  String title;
  String points;
  DateTime date;
  String value;

  PurchasedReward({
    this.subTitle,
    this.title,
    this.points,
    this.date,
    this.value,
  });

  void setSubTitle(String newSubTitle) {
    if (newSubTitle == null || newSubTitle == '') {
      subTitle = '';
    } else
      subTitle = newSubTitle;
  }

  void setDate(String newDate) {
    if (newDate == null || newDate == '') {
      date = DateTime.fromMillisecondsSinceEpoch(0);
    } else
      date = DateTime.fromMillisecondsSinceEpoch(int.parse(newDate));
  }

  void setTitle(String newTitle) {
    if (newTitle == null || newTitle == '') {
      title = '';
    } else
      title = newTitle;
  }

  void setPoints(String newPoints) {
    if (newPoints == null || newPoints == '') {
      points = '99999999';
    } else if (!(RegExp('^[0-9]*\$').hasMatch(newPoints))) {
      throw FormatException('Points must be numeric');
    } else
      points = newPoints;
  }

  void setValue(String newValue) {
    if (newValue == null) {
      throw FormatException('''Null 'value' field''');
    } else if (newValue == '') {
      throw FormatException('''Empty 'value' field''');
    } else if (!(RegExp('^[0-9]*\$').hasMatch(newValue))) {
      throw FormatException(''''Value' field must be numeric''');
    } else
      value = newValue;
  }

  PurchasedReward.fromJson(Map<String, dynamic> json) {
    setSubTitle(json['subTitle']);
    setTitle(json['title']);
    setPoints(json['points']);
    setDate(json['deliveryDate']);
    setValue(json['value']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subTitle'] = this.subTitle;
    data['title'] = this.title;
    data['points'] = this.points;
    data['deliveryDate'] = this.date.millisecondsSinceEpoch.toString();
    data['value'] = this.value;
    return data;
  }
}
