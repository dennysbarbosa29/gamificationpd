import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gamification/services/auth.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/ui/controllers/versioncheckprovider.dart';
import 'package:gamification/ui/widgets/version_dialogs.dart';
import 'package:provider/provider.dart';

import '../router.dart';

class SplashScreenView extends StatelessWidget {

  BuildContext context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    startSplashScreenTimer();
    Future<DocumentSnapshot> auth =
        AuthenticationServices.isUserAuthenticated();

    return Consumer<VersionCheck>(
      builder: (context, VersionCheck versionCheck, _) {
        
        return Scaffold(
          body: Center(
            child: Container(
              height: 300,
              width: 300,
              //color: Colors.black,
              //alignment: Alignment.center,
                child: Container(
                    child: new Image.asset("assets/images/logo_open_labs.png")
                )
//                FlareActor(
//                "assets/images/logo_open_labs.png",
//                alignment: Alignment.center,
//                //color : Colors.black,
//                fit: BoxFit.contain,
//                animation: "go",
//                callback: (_) async {
//                  DocumentSnapshot snapshot = await auth;
//
//                   if (snapshot == null) {
//                    Navigator.pushReplacementNamed(context, loginRoute);
//                  }
//                   else {
//                     //User.email = snapshot.data['email'];
//                  //   User.uid = snapshot.data['uid'];
//                  //   User.displayName = snapshot.data['displayName'];
//                    //TODO: Diogo: CRIAR SPLASH PROVIDER
//                    HomeScreenController homeScreenController =
//                      Provider.of<HomeScreenController>(context);
//                    homeScreenController.loadGames();
//                    Navigator.pushReplacementNamed(context, homeRoute);
//                  }
//                  if (versionCheck.getShowDialog) {
//                    showVersionDialog(
//                        context: context,
//                        forceAsk: versionCheck.getForceAsk,
//                        updateTap: versionCheck.launchURL);
//                  }
//                },
//              ),
            ),
          ),
        );
      },
    );
  }
  startSplashScreenTimer() async{

    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, _navigationNextPage);
  }

  _navigationNextPage(){
    Navigator.pushReplacementNamed(context, loginRoute);
  }
}
