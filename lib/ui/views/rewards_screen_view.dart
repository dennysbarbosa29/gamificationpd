import 'package:flutter/material.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/ui/widgets/details_card.dart';
import 'package:gamification/ui/widgets/details_screens_canvas.dart';
import 'package:gamification/ui/widgets/slidable_tile.dart';
import 'package:gamification/ui/widgets/slidable_tiles_renderer.dart';
import 'package:provider/provider.dart';

class RewardsScreenView extends StatelessWidget {
  RewardsScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String _appBarTitle = 'Recompensas';
    return Consumer<HomeScreenController>(
        builder: (context, HomeScreenController homeScreenController, _) {
          return DetailsScreenCanvas(
            screenTitle: _appBarTitle,
            cardWidget: DetailsCard(
              cardType: CardType.Points,
              secondBalloonContent: User.points,
              // thirdBalloonContent: homeScreenController.selectedGame.totalRewardPoints,
            ),
            listTitle: 'Recompensas',
            bodyWidget: homeScreenController.state == ViewState.Busy
            ? Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Center(child: CircularProgressIndicator()),
            )
            : homeScreenController.state == ViewState.Error
            ? Stack(
              children: <Widget>[ 
                GestureDetector(
                  onTap: homeScreenController.clearErrorState,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.6,
                    color: Colors.transparent,
                  ),
                ),
                AlertDialog(
                  title: Center(child: Text('Erro')),
                  content: Center(child:Text(homeScreenController.errorMessage)),
                  elevation: 3.0,
                ),
              ],
            )
            : RenderTiles(
              content: homeScreenController.selectedGame.rewardsMap,
              tileType: TileType.Rewards,
              controller: homeScreenController,
            ),
            onRefresh: () async {
              homeScreenController.shouldReloadGames = true;
              await homeScreenController.loadGames();
            },
          );
        },
    );
  }
}
