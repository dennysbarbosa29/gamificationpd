import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/services/auth.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/ui/controllers/profile_picture_controller.dart';
import 'package:gamification/ui/widgets/login_singup_background.dart';
import 'package:gamification/ui/widgets/login_singup_field.dart';
import 'package:gamification/utils/app_colors.dart';
import 'package:gamification/utils/custom_icons.dart';
import 'package:gamification/utils/dimensions.dart';
import 'package:gamification/utils/validators.dart';
import 'package:provider/provider.dart';

import '../controllers/login_screen_controller.dart';
import '../router.dart';
import 'base_view.dart';

class LoginScreenView extends StatefulWidget {
  LoginScreenView({Key key}) : super(key: key);

  @override
  _LoginScreenViewState createState() => _LoginScreenViewState();
}

class _LoginScreenViewState extends State<LoginScreenView> {
  FocusNode _emailFocus;
  FocusNode _passwordFocus;
  FocusNode _nameFocus;
  FocusNode _phoneNumberFocus;
  FocusNode _registrationFocus;
  FocusNode _confirmPasswordFocus;
  FocusNode _resetPasswordFocus;

  final TextStyle style = TextStyle(fontSize: 20.0);

  @override
  void initState() {
    _emailFocus = FocusNode();
    _passwordFocus = FocusNode();
    _nameFocus = FocusNode();
    _phoneNumberFocus = FocusNode();
    _registrationFocus = FocusNode();
    _confirmPasswordFocus = FocusNode();
    _resetPasswordFocus = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _emailFocus.dispose();
    _passwordFocus.dispose();
    _nameFocus.dispose();
    _phoneNumberFocus.dispose();
    _registrationFocus.dispose();
    _confirmPasswordFocus.dispose();
    super.dispose();
  }

  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();

  void _singInSingUp(LoginScreenController loginScreenController) {
    loginScreenController.handleSingInSingUp().then(
      (result) {
        if (result == null) {
          _showDialog(loginScreenController);
        } else if (result.status) {
          // Set user id in profile picture provider
          // TODO: change this to a proxy provider
          ProfilePictureController profilePictureController =
              Provider.of<ProfilePictureController>(context);
          profilePictureController.setUid(User.uid);

          if (loginScreenController.formType == FormType.SingUp) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return _buildErrorDialog(
                      context, loginScreenController, result);
                });

            // Can't login automatically after creating account because the email should be confirmed
            loginScreenController.formType = FormType.Login;
          } else if (loginScreenController.formType == FormType.Login) {
            // Go to home screen
            HomeScreenController homeScreenController =
                Provider.of<HomeScreenController>(context);
            homeScreenController.loadGames();
            Navigator.pushReplacementNamed(context, homeRoute);
          }
        } else if (!result.status) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return _buildErrorDialog(context, loginScreenController, result);
            },
          );
        }
      },
    );
  }

  void _resetPassword(LoginScreenController loginScreenController) {
    loginScreenController.resetPassword().then((result) {
      if (result == null)
        return null;
      else if (result.status) {
        loginScreenController.formType = FormType.Login;
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: new Text(
                  "Siga as instruções contidas no email enviado para o endereço ${loginScreenController.resetPasswordEmail} para redefinir sua senha."),
              actions: <Widget>[
                // define os botões na base do dialogo
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } else
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // retorna um objeto do tipo Dialog
            return AlertDialog(
              content: result.errorCode == 'ERROR_USER_NOT_FOUND'
                  ? Text(
                      'O endereço de email inserido não está não cadastrado na plataforma.')
                  : Text(
                      "Ocorreu um erro na operação. Tente novamente mais tarde."),
              actions: <Widget>[
                new FlatButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
    });
  }

  Widget _buildErrorDialog(BuildContext context,
      LoginScreenController controller, AuthenticationResult result) {
    List<Widget> buttons = [];
    String message = '';

    if (result.errorCode == 'ERROR_EMAIL_NOT_VERIFIED') {
      message =
          'Acesse o link enviado para o email cadastrado para confirmar sua conta.';
      buttons.add(
        FlatButton(
          child: Text("Reenviar"),
          onPressed: () async {
            await controller.resendVerificationEmail();
            Navigator.pop(context);
          },
        ),
      );
    } else if (result.errorCode == 'ERROR_USER_NOT_FOUND' ||
        result.errorCode == 'ERROR_WRONG_PASSWORD') {
      message = 'Usuário e/ou senha incorretos.';
    } else {
      if (controller.formType == FormType.Login) {
        message = result.errorMessage;
      } else {
        message = result.errorMessage;
      }
    }

    buttons.add(
      FlatButton(
        child: Text("Fechar"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    );

    return AlertDialog(
      title: Center(
          child: result.errorCode == '201'
              ? Text(
                  "Alerta",
                  style: TextStyle(color: Colors.grey),)
              : Text("Erro", style: TextStyle(color: Colors.grey)),
                ),
      content: Text(
        message,
        style: TextStyle(
          fontSize: 14,
        ),
      ),
      actions: buttons,
    );
  }

  void _showDialog(LoginScreenController loginScreenController) {
    loginScreenController.handleSingInSingUp().then((result) {
      if (result == null)
        return null;
      else if (result.status)
        Navigator.pushReplacementNamed(context, homeRoute);
      else if (!result.status) //FIXME : Snackbar not fiding context
        debugPrint(result.errorCode);
      debugPrint(result.errorMessage);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            title: new Text("Alert Dialog titulo"),
            content: new Text("Alert Dialog body"),
            actions: <Widget>[
              // define os botões na base do dialogo
              new FlatButton(
                child: new Text("Fechar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    });
  }

  List<Widget> _buildInputs(
      LoginScreenController loginScreenController, BuildContext context) {
    List<Widget> emailPasswordInputs = [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Email',
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.next,
          validator: FieldValidators.validateEmail,
          onSaved: (value) => loginScreenController.email = value,
          focusNode: _emailFocus,
          //initValue: "lucas-c-macedo@openlabs.com.br",
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_passwordFocus);
          },
        ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          isPasswordField: true,
          hintText: 'Senha',
          keyboardType: TextInputType.visiblePassword,
          textInputAction: TextInputAction.done,
          validator: FieldValidators.validatePwd,
          onSaved: (value) => loginScreenController.password = value,
          focusNode: _passwordFocus,
          style: style,
          //initValue: "666666",
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            if (loginScreenController.formType == FormType.Login)
              _singInSingUp(loginScreenController);
            else
              FocusScope.of(context).requestFocus(_nameFocus);
          },
        ),
      ),
    ];

    List<Widget> signUpInputs = [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Nome',
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.next,
          onSaved: (value) => loginScreenController.name = value,
          focusNode: _nameFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_emailFocus);
          },
        ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Email',
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.next,
          validator: FieldValidators.validateEmail,
          onSaved: (value) => loginScreenController.email = value,
          focusNode: _emailFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_registrationFocus);
          },
        ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      // Padding(
      //   padding: const EdgeInsets.symmetric(horizontal: 8.0),
      //   child: LoginSingUpField(
      //     hintText: 'Número Celular',
      //     keyboardType: TextInputType.number,
      //     textInputAction: TextInputAction.done,
      //     onSaved: (value) => loginScreenController.phoneNumber = value,
      //     focusNode: _phoneNumberFocus,
      //     style: style,
      //     onFieldSubmitted: (_) {
      //       FocusScope.of(context).unfocus();
      //       _singInSingUp(loginScreenController);
      //     },
      //   ),
      // ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Matricula',
          keyboardType: TextInputType.number,
          textInputAction: TextInputAction.done,
          validator: FieldValidators.validateMatricula,
          onSaved: (value) => loginScreenController.registration = value,
          focusNode: _registrationFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_passwordFocus);
          },
        ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: TextFormField(
          style: style,
          textAlign: TextAlign.center,
          obscureText: true,
          focusNode: _passwordFocus,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_confirmPasswordFocus);
          },
          controller: _pass,
          validator: FieldValidators.validatePwd,
          onSaved: (value) => loginScreenController.password = value,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: 'Senha',
            fillColor: Colors.white,
            filled: true,
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
            errorStyle: TextStyle(fontSize: 11.0),
          ),
        ),

        // child: LoginSingUpField(
        //   isPasswordField: true,
        //   hintText: 'Senha',
        //   keyboardType: TextInputType.visiblePassword,
        //   textInputAction: TextInputAction.done,
        //   validator: FieldValidators.validatePwd,
        //   onSaved: (value) => loginScreenController.password = value,
        //   focusNode: _passwordFocus,
        //   style: style,
        //   onFieldSubmitted: (_) {
        //     FocusScope.of(context).unfocus();
        //     FocusScope.of(context).requestFocus(_confirmPasswordFocus);
        //   },
        // ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: TextFormField(
          style: style,
          textAlign: TextAlign.center,
          obscureText: true,
          focusNode: _confirmPasswordFocus,
          controller: _confirmPass,
          onSaved: (value) => loginScreenController.confirmPassword = value,
          validator: (val) {
            if (FieldValidators.validatePwd(val) != null)
              return 'Senha deve ter entre 6 e 12 caracteres';
            if (val != _pass.text) return 'Senha e Confirmar não são iguais';
            return null;
          },
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            //loginScreenController.validatePwdsMatch();
            _singInSingUp(loginScreenController);
          },
          decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: 'Confirmar Senha',
            fillColor: Colors.white,
            filled: true,
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
            errorStyle: TextStyle(fontSize: 11.0),
          ),
        ),

        // child: LoginSingUpField(
        //   isPasswordField: true,
        //   hintText: 'Confirmar Senha',
        //   keyboardType: TextInputType.visiblePassword,
        //   textInputAction: TextInputAction.done,
        //   validator: (value) {
        //     if (loginScreenController.password != loginScreenController.confirmpassword) {
        //       debugPrint('senha: ${loginScreenController.password}');
        //       debugPrint('confirmação: ${loginScreenController.confirmpassword}');
        //       return 'Senhas devem ser iguais';
        //     }
        //     return null;
        //   },
        //   onSaved: (value) => loginScreenController.confirmPassword = value,
        //   focusNode: _confirmPasswordFocus,
        //   style: style,
        //   onFieldSubmitted: (_) {
        //     FieldValidators.validatePwdsMatch(
        //         loginScreenController.confirmpassword,
        //         loginScreenController.password);
        //     FocusScope.of(context).unfocus();
        //     _singInSingUp(loginScreenController);
        //   },
        // ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
    ];

    List<Widget> resetPasswordInput() {
      return [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextFormField(
            style: style,
            textAlign: TextAlign.center,
            focusNode: _resetPasswordFocus,
            onFieldSubmitted: (_) {
              FocusScope.of(context).unfocus();
              _resetPassword(loginScreenController);
            },
            validator: FieldValidators.validateEmail,
            onSaved: (value) =>
                loginScreenController.resetPasswordEmail = value,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: 'Email',
              fillColor: Colors.white,
              filled: true,
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
              errorStyle: TextStyle(fontSize: 11.0),
            ),
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      ];
    }

    return loginScreenController.formType == FormType.Login
        ? emailPasswordInputs
        : loginScreenController.formType == FormType.SingUp
            ? [
                //...emailPasswordInputs,
                Dimensions.heightSpacer(
                    Dimensions.screenHeight(context) * 0.01),
                ...signUpInputs
              ]
            : resetPasswordInput();
  }

  List<Widget> _buildButtons(
      LoginScreenController loginScreenController, BuildContext context) {
    Material signInButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.blue, //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          _singInSingUp(loginScreenController);
        },
        child: loginScreenController.state == ViewState.Idle
            ? Text("Entrar",
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold))
            : SizedBox(
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ),
      ),
    );
    Material facebookButton = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(8.0),
        color: Color.fromARGB(
            255, 66, 103, 178), //TODO: Diogo: use themedata buttoncolor
        child: MaterialButton(
          padding: EdgeInsets.symmetric(horizontal: 20),
          onPressed: () async {
            loginScreenController.facebookSignIn(context);
          },
          child: Row(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                color: Color.fromARGB(255, 66, 103, 178),
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: Icon(
                  CustomIcons.facebook_official,
                  color: Colors.white.withOpacity(0.95),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Text("Continuar com Facebook",
                  textAlign: TextAlign.end,
                  style: style.copyWith(
                      color: Colors.white, fontWeight: FontWeight.bold)),
            ],
          ),
        ));
    Material googleButton = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white, //TODO: Diogo: use themedata buttoncolor
        child: MaterialButton(
          padding: EdgeInsets.symmetric(horizontal: 20),
          onPressed: () async {
            loginScreenController.googleSignIn(context);
          },
          child: Row(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                color: Colors.white,
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: Image.asset('assets/images/google_logo.png'
                    //color: Colors.white.withOpacity(0.95),
                    ),
              ),
              SizedBox(
                width: 20,
              ),
              Text("Continuar com Google",
                  textAlign: TextAlign.end,
                  style: style.copyWith(
                      color: Colors.black45, fontWeight: FontWeight.bold)),
            ],
          ),
        ));

    Material signUpButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.blue, //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          _singInSingUp(loginScreenController);
        },
        child: loginScreenController.state == ViewState.Idle
            ? Text("Cadastrar",
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold))
            : SizedBox(
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ),
      ),
    );

    Material resetPasswordButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.blue, //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          _resetPassword(loginScreenController);
        },
        child: loginScreenController.state == ViewState.Idle
            ? Text("Enviar Email",
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold))
            : SizedBox(
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ),
      ),
    );

    Material backButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(500),
      color: Colors.grey[600], //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        minWidth: 20,

        onPressed: () async {
          FocusScope.of(context).unfocus();
          loginScreenController.formType = FormType.Login;
        },
        child: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        // child: Text("Voltar",
        //     textAlign: TextAlign.center,
        //     style: style.copyWith(
        //       color: Colors.white,
        //       fontWeight: FontWeight.bold,
        //     )),
      ),
    );

    Material newAccButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.grey[600], //TODO: Diogo: use themedata
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          loginScreenController.formType = FormType.SingUp;
        },
        child: Text("Criar Nova Conta",
            textAlign: TextAlign.center,
            style: style.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            )),
      ),
    );

    if (loginScreenController.formType == FormType.Login)
      return [
        //Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.02),
        Container(
          height: Dimensions.screenHeight(context) * 0.07,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 0),
            child: signInButton,
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.005),
        Container(
          height: Dimensions.screenHeight(context) * 0.09,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Não tem uma conta?',
                textAlign: TextAlign.left,
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              FlatButton(
                  textColor: Colors.blue,
                  onPressed: () async {
                    FocusScope.of(context).unfocus();
                    loginScreenController.formType = FormType.SingUp;
                  },
                  child: Text("Inscreva-se", textAlign: TextAlign.left)),
            ],
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.02),
        //facebookButton,
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
        //googleButton,
        Dimensions.heightSpacer(Dimensions.inBetweenItensPadding * 2),
        // HeightSpacerWithText(
        //   text: 'Ou',
        //   textSize: 17,
        // ),
        // Dimensions.heightSpacer(Dimensions.inBetweenItensPadding * 2),
        // Padding(
        //   padding: const EdgeInsets.only(bottom: 15.0),
        //   child: LoginSocialButtons(
        //     googleOnPressed: () => loginScreenController.googleSignIn(context),
        //     facebookOnPressed: () {
        //       print('apertou');
        //       loginScreenController.facebookSignIn(context);
        //     },
        //   ),
        // ),
      ];
    else
      return [
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
        Container(
          //alignment: Alignment.centerRight,
          height: Dimensions.screenHeight(context) * 0.07,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: loginScreenController.formType == FormType.SingUp
                ? signUpButton
                : resetPasswordButton,
          ),
        ),
        SizedBox(
          height: 50,
        )
      ];
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginScreenController>(
      controller: LoginScreenController(),
      builder: (context, LoginScreenController loginScreenController, _) {
        return LoginSingUpBackground(
          child: WillPopScope(
            onWillPop: () async {
              if (loginScreenController.formType != FormType.Login) {
                loginScreenController.formType = FormType.Login;
                return false;
              } else
                return true;
            },
            child: AnnotatedRegion<SystemUiOverlayStyle>(
              value: SystemUiOverlayStyle(
                statusBarIconBrightness: Brightness.dark,
                statusBarColor: Colors.transparent,
              ),
              child: Scaffold(
                backgroundColor: AppColors.backgroundLightBlue,
                body: Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: Dimensions.toppadding,
                            left: Dimensions.sidePadding,
                            right: Dimensions.sidePadding),
                        child: Form(
                          key: loginScreenController.formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                fit: BoxFit.contain,
                                image: AssetImage(
                                  'assets/images/logo_open_labs.png',
                                ),
                              ),
                              Dimensions.heightSpacer(
                                  Dimensions.inBetweenItensPadding * 2.5),
                              Card(
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      height:
                                          Dimensions.screenHeight(context) * 0.06,
                                      width: Dimensions.screenWidth(context),
                                      decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        shape: BoxShape.rectangle,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            topRight: Radius.circular(8)),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: FittedBox(
                                            fit: BoxFit.fitHeight,
                                            child: Text(
                                              loginScreenController.formType ==
                                                      FormType.Login
                                                  ? 'Login'
                                                  : loginScreenController
                                                              .formType ==
                                                          FormType.SingUp
                                                      ? 'Cadastrar'
                                                      : 'Redefinir Senha',
                                              style: style.copyWith(
                                                  color: Colors.black54,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      ),
                                      //color: Colors.grey,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 8, right: 8),
                                      child: SizedBox(
                                          height:
                                              Dimensions.screenHeight(context) *
                                                  0.01),
                                    ),
                                    Column(
                                      children: _buildInputs(
                                          loginScreenController, context),
                                    ),
                                    loginScreenController.formType ==
                                            FormType.Login
                                        ? Container(
                                            height:
                                                Dimensions.screenHeight(context) *
                                                    0.05,
                                            width:
                                                Dimensions.screenWidth(context) /
                                                    0.12,
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.only(right: 8),
                                                    child: FlatButton(
                                                        textColor: Colors.blue,
                                                        onPressed: () {
                                                          loginScreenController
                                                                  .formType =
                                                              FormType
                                                                  .ForgotPassword;
                                                          setState(() {});
                                                        },
                                                        child: Text(
                                                            "Esqueceu sua Senha?",
                                                            textAlign:
                                                                TextAlign.end)),
                                                  ),
                                                ]),
                                          )
                                        : Dimensions.heightSpacer(
                                            Dimensions.screenHeight(context) *
                                                0.01),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: Dimensions.screenHeight(context) * 0.015,
                              ),
                              ..._buildButtons(loginScreenController, context),
                            ],
                          ),
                        ),
                      ),
                    ),
                    loginScreenController.formType == FormType.Login
                        ? Container()
                        : SafeArea(
                            child: Container(
                              alignment: Alignment.topLeft,
                              color: Colors.transparent,
                              height: 46,
                              width: 46,
                              child: IconButton(
                                color: Colors.grey[600],
                                onPressed: () {
                                  loginScreenController.formType = FormType.Login;
                                },
                                icon: Icon(Icons.arrow_back),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
