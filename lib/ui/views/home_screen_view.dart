import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gamification/models/game_model.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/ui/widgets/app_card.dart';
import 'package:gamification/ui/widgets/app_list_tile.dart';
import 'package:gamification/ui/widgets/double_string_column.dart';
import 'package:gamification/ui/widgets/profile_picture.dart';
import 'package:gamification/utils/app_colors.dart';
import 'package:gamification/utils/dimensions.dart';
import 'package:gamification/utils/formatters.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:provider/provider.dart';

import '../router.dart';

final Formatters formatters = Formatters();

// Reference -> https://medium.com/flutter/decomposing-widgets-backdrop-b5c664fb9cf4
class HomeScreenView extends StatelessWidget {
  HomeScreenView({Key key}) : super(key: key);
//TODO: show a nice screen loader while the controller is fetching the data
  @override
  Widget build(BuildContext context) {
    // final String _frontPanelClosedTitle = 'Abrir';
    // final frontPanelIsVisible = ValueNotifier<bool>(false);
    //HomeScreenController homeScreenController = HomeScreenController();
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        backgroundColor: AppColors.backgroundLightBlue,
        body: Column(
          children: <Widget>[
            SizedBox(height: Dimensions.screenHeight(context) * 0.05),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 15, 20, 5),
              child: Container(
                height: Dimensions.screenHeight(context) * 0.15,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  //shape: BoxShape.circle,
                  image: DecorationImage(
                    fit: BoxFit.contain,
                    image: AssetImage('assets/images/logo-full-open-labs.png'),
                  ),
                ),
              ),
            ),
            //SizedBox(height: Dimensions.screenHeight(context)*0.005),
            FrontPanel(),
          ],
        ),
      ),
    );
  }
}

class FrontPanel extends StatelessWidget {
  FrontPanel({Key key}) : super(key: key);

  //Front Panel Strings Defs
  final String _rankingString = 'Ranking';
  final String _myRewardPointsString = 'Meus créditos';
  final String _missionCardTitle = 'Mission';
  final String _informationIconTootltip = 'Informations';
  final String _rewardsTileText = 'Recompensas';
  final String _purchaseHistoryTileText = 'Histórico';
  final String _missionsTileText = 'Missões';
  final String _logoutTileText = 'Sair';

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenController>(
      builder:
          (BuildContext context, HomeScreenController homeScreenController, _) {
        DateTime currentBackPressTime;
        return WillPopScope(
          onWillPop: () {
            DateTime now = DateTime.now();
            if (currentBackPressTime == null ||
                now.difference(currentBackPressTime) > Duration(seconds: 2)) {
              currentBackPressTime = now;
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  behavior: SnackBarBehavior.floating,
                  duration: Duration(seconds: 2),
                  content: Text("Pressione novamente para sair"),
                ),
              );
              return Future.value(false);
            }
            return Future.value(true);
          },
          child: Flexible(
            child: homeScreenController.state == ViewState.Busy
                ? Center(child: CircularProgressIndicator())
                : homeScreenController.state == ViewState.Error
                    ? Center(
                        child: User.active
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'Erro ao carregar as informações. ${homeScreenController.errorMessage}',
                                  textAlign: TextAlign.center,
                                ),
                              )
                            : Text(
                                homeScreenController.errorMessage,
                                textAlign: TextAlign.center,
                              ),
                      )
                    : Padding(
                        padding: const EdgeInsets.only(
                          top: 8.0,
                          right: Dimensions.smallPadding,
                          left: Dimensions.smallPadding,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              AppCard(
                                child: Column(
                                  children: <Widget>[
                                    InkWell(
                                      borderRadius: BorderRadius.circular(5),
                                      onTap: () => Navigator.pushNamed(
                                          context, perfilDetailRoute),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          AvatarPictureShow(),
                                          // Container(
                                          //   height: Dimensions.profilePictureSize,
                                          //   width: Dimensions.profilePictureSize,
                                          //   decoration: BoxDecoration(
                                          //     shape: BoxShape.circle,
                                          //     image: DecorationImage(
                                          //       fit: BoxFit.fill,
                                          //       image: AssetImage(_profilePictureImage),
                                          //     ),
                                          //   ),
                                          // ),
                                          Dimensions.widthSpacer(),
                                          Expanded(
                                            child: Text(
                                              User.displayName,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .body1,
                                              overflow: TextOverflow.fade,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Dimensions.heightSpacer(
                                        Dimensions.smallPadding * 2),
                                    Divider(
                                      thickness: 1.5,
                                      indent: Dimensions.smallPadding,
                                      endIndent: Dimensions.smallPadding,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Flexible(
                                          flex: 10,
                                          child: InkWell(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            child: Container(
                                              height: 70,
                                              color: Colors.transparent,
                                              alignment: Alignment.center,
                                              child: DoubleStringColumn(
                                                string1: _rankingString,
                                                string2:
                                                    User.ranking.toString() +
                                                        '°',
                                              ),
                                            ),
                                            onTap: () {
                                              Navigator.pushNamed(
                                                  context, userRankingRoute);
                                            },
                                          ),
                                        ),
                                        Flexible(
                                          flex: 1,
                                          child: Container(
                                            //Simulates a vertical divider. Idk why the native verticalDivider doesnt work
                                            width: 2.1,
                                            height: 70,
                                            color:
                                                Theme.of(context).dividerColor,
                                          ),
                                        ),
                                        Flexible(
                                          flex: 10,
                                          child: Container(
                                            height: 70,
                                            color: Colors.transparent,
                                            alignment: Alignment.center,
                                            child: DoubleStringColumn(
                                              string1: _myRewardPointsString,
                                              string2: formatters
                                                  .intWithDot(User.points),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              // AppCard(
                              //   child: Column(
                              //     children: <Widget>[
                              //       Row(
                              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //         children: <Widget>[
                              //           Text(
                              //             _missionCardTitle,
                              //             style: Theme.of(context).textTheme.title,
                              //           ),
                              //           IconButton(
                              //             icon: Icon(
                              //               Icons.info_outline,
                              //               color: Colors.black,
                              //               size: 32,
                              //             ),
                              //             onPressed: () => null,
                              //             tooltip: _informationIconTootltip,
                              //           ),
                              //         ],
                              //       ),
                              //       MissionProgressIndicator(
                              //         desiredProgressMarkerColor:
                              //             AppColors.desiredProgressMarker,
                              //         goalOfTheDayMarkerColor:
                              //             AppColors.goalOfTheDayMarker,
                              //         currentProgress: homeScreenController
                              //             .selectedGame.mission.currentProgress,
                              //         desiredProgress: homeScreenController
                              //             .selectedGame.mission.desiredProgress,
                              //         goalOfTheDay: homeScreenController
                              //             .selectedGame.mission.goalOfTheDay,
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              // Dimensions.heightSpacer(),
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              //   crossAxisAlignment: CrossAxisAlignment.start,
                              //   children: <Widget>[
                              //     GestureDetector(
                              //       onTap: () {
                              //         Navigator.pushNamed(context, missionsScreenRoute);
                              //       },
                              //       child: Container(
                              //         width: Dimensions.screenWidth(context)*0.20,
                              //         height: 80,
                              //         child: Column(
                              //           //mainAxisAlignment: MainAxisAlignment.center,
                              //           children: <Widget>[
                              //             Icon(Icons.access_time, size: 40),
                              //             Text(_missionsTileText,
                              //             textAlign: TextAlign.center,
                              //                 style: Theme.of(context)
                              //                     .textTheme
                              //                     .subhead
                              //                     .copyWith(color: Colors.black))
                              //           ],
                              //         ),
                              //       ),
                              //     ),
                              //     Container(height: 50,
                              //       width: 1.5,
                              //       color: Colors.grey[500],
                              //     ),
                              //     GestureDetector(
                              //       onTap: () {
                              //         Navigator.pushNamed(context, rewardsRoute);
                              //       },
                              //       child: Container(
                              //         width: Dimensions.screenWidth(context)*0.20,
                              //         height: 80,
                              //         child: Column(
                              //           //mainAxisAlignment: MainAxisAlignment.center,
                              //           children: <Widget>[
                              //             Icon(Icons.card_giftcard, size: 40),
                              //             FittedBox(
                              //               fit: BoxFit.fitWidth,
                              //               child: Text(
                              //                 _rewardsTileText,
                              //                 textAlign: TextAlign.center,
                              //                   style: Theme.of(context)
                              //                       .textTheme
                              //                       .subhead
                              //                       .copyWith(color: Colors.black)),
                              //             )
                              //           ],
                              //         ),
                              //       ),
                              //     ),
                              //     Container(height: 50,
                              //       width: 1.5,
                              //       color: Colors.grey[500],
                              //     ),
                              //     GestureDetector(
                              //       onTap: () {
                              //         Navigator.pushNamed(
                              //           context, purchaseHistoryRoute);
                              //       },
                              //       child: Container(
                              //         width: Dimensions.screenWidth(context)*0.20,
                              //         height: 80,
                              //         child: Column(
                              //           //mainAxisAlignment: MainAxisAlignment.center,
                              //           children: <Widget>[
                              //             Icon(Icons.loyalty, size: 40),
                              //             Text(_purchaseHistoryTileText,
                              //             textAlign: TextAlign.center,
                              //                 style: Theme.of(context)
                              //                     .textTheme
                              //                     .subhead
                              //                     .copyWith(color: Colors.black))
                              //           ],
                              //         ),
                              //       ),
                              //     ),
                              //     Container(height: 50,
                              //       width: 1.5,
                              //       color: Colors.grey[500],
                              //     ),
                              //     GestureDetector(
                              //       onTap: () {
                              //         AuthenticationServices.userLogout();
                              //         Navigator.pushReplacementNamed(
                              //             context, loginRoute);
                              //       },
                              //       child: Container(
                              //         width: Dimensions.screenWidth(context)*0.20,
                              //         height: 80,
                              //         child: Column(
                              //           //mainAxisAlignment: MainAxisAlignment.center,
                              //           children: <Widget>[
                              //             Icon(Icons.exit_to_app, size: 40),
                              //             Text('Logout',
                              //             textAlign: TextAlign.center,
                              //                 style: Theme.of(context)
                              //                     .textTheme
                              //                     .subhead
                              //                     .copyWith(color: Colors.black))
                              //           ],
                              //         ),
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              Dimensions.heightSpacer(),
                              AppListTile(
                                leadingIcon: Icons.access_time,
                                text: _missionsTileText,
                                routeName: missionsScreenRoute,
                              ),
                              Divider(
                                thickness: 1.5,
                              ),
                              AppListTile(
                                leadingIcon: Icons.card_giftcard,
                                text: _rewardsTileText,
                                routeName: rewardsRoute,
                              ),
                              Divider(
                                thickness: 1.5,
                              ),
                              AppListTile(
                                leadingIcon: Icons.loyalty,
                                text: _purchaseHistoryTileText,
                                routeName: purchaseHistoryRoute,
                              ),
                              Divider(
                                thickness: 1.5,
                              ),
                              // AppListTile(
                              //   leadingIcon: Icons.exit_to_app,
                              //   text: _logoutTileText,
                              //   routeName: loginRoute,
                              // ),
                              Dimensions.heightSpacer(Dimensions.smallPadding),
                            ],
                          ),
                        ),
                      ),
          ),
        );
      },
    );
  }
}

class BackPanel extends StatelessWidget {
  BackPanel({
    Key key,
    @required this.frontPanelIsVisible,
  }) : super(key: key);
  final ValueNotifier<bool> frontPanelIsVisible;

  final pageIndexNotifier = ValueNotifier<int>(0);
  final String _genericErrorMessage = 'Ocorreu um erro ao carregar os dados.';
  final String _startDateText = 'De ';
  final String _endDateText = 'até ';

  Widget _showGameBanner(BuildContext context,
      ValueNotifier<bool> frontPanelIsVisible, Game game) {
    //TODO : use a layout builder to define this height, maybe put this inside the backdrop def?
    final double _bannerBottomPaddingWhenFrontPanelIsVisible =
        (Dimensions.frontPanelOpenPercentage *
                Dimensions.screenHeight(context)) -
            Dimensions.screenHeight(context) * 0.25;
    return ValueListenableBuilder(
      valueListenable: frontPanelIsVisible,
      builder: (context, _frontPanelIsVisible, _) {
        return AnimatedContainer(
          duration: Duration(milliseconds: 250),
          curve: Curves.linearToEaseOut,
          margin: frontPanelIsVisible.value
              ? EdgeInsets.only(
                  bottom: _bannerBottomPaddingWhenFrontPanelIsVisible)
              : const EdgeInsets.only(
                  top: 60.0,
                  left: Dimensions.bigPadding,
                  right: Dimensions.bigPadding,
                ),
          decoration: BoxDecoration(
            color: Colors.white70,
            image: DecorationImage(
              fit: BoxFit.fitWidth,
              image: AssetImage(game.imagePath),
            ),
          ),
          child: Column(
            children: <Widget>[
              Dimensions.heightSpacer(Dimensions.mediumPadding),
              if (!_frontPanelIsVisible)
                Text(
                  game.name,
                  style: Theme.of(context).textTheme.display1,
                ),
              Dimensions.heightSpacer(Dimensions.smallPadding),
              if (!_frontPanelIsVisible)
                // Text(
                //   formatters.date(game.startDate) +
                //   _datesConectorString +
                //   formatters.date(game.endDate),
                //   style: Theme.of(context).textTheme.subhead,
                // ),
                Text(
                  _startDateText +
                      formatters.dateAndTime(game.startDate) +
                      '\n' +
                      _endDateText +
                      formatters.dateAndTime(game.endDate),
                  style: Theme.of(context).textTheme.subhead,
                ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildErrorCard(String message) {
    return Center(
      child: AlertDialog(
        title: Center(child: Text('Erro')),
        content: Text('$_genericErrorMessage $message'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenController>(
      builder: (context, HomeScreenController homeScreenController, _) {
        return AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle(
            statusBarIconBrightness: Brightness.dark,
            statusBarColor: Colors.transparent,
          ),
          child: Scaffold(
            body: homeScreenController.state == ViewState.Busy
                ? Center(child: CircularProgressIndicator())
                : homeScreenController.state == ViewState.Error
                    ? _buildErrorCard(homeScreenController.errorMessage)
                    : Column(
                        children: <Widget>[
                          Flexible(
                            child: PageView.builder(
                              onPageChanged: (index) {
                                pageIndexNotifier.value = index;
                                homeScreenController.setSelectedGame(index);
                              },
                              itemCount: homeScreenController.numberOfGames,
                              itemBuilder: (context, index) {
                                return _showGameBanner(
                                    context,
                                    frontPanelIsVisible,
                                    homeScreenController.selectedGame);
                              },
                            ),
                          ),
                          Dimensions.heightSpacer(Dimensions.smallPadding),
                          PageViewIndicator(
                            pageIndexNotifier: pageIndexNotifier,
                            length: homeScreenController.numberOfGames,
                            normalBuilder: (animationController, index) => Circle(
                              size: 8.0,
                              color: AppColors.pageViewUnselectedIndicator,
                            ),
                            highlightedBuilder: (animationController, index) =>
                                ScaleTransition(
                              scale: CurvedAnimation(
                                parent: animationController,
                                curve: Curves.ease,
                              ),
                              child: Circle(
                                size: 12.0,
                                color: AppColors.pageViewSelectedIndicator,
                              ),
                            ),
                          ),
                          Dimensions.heightSpacer(Dimensions.mediumPadding),
                        ],
                      ),
          ),
        );
      },
    );
  }
}
