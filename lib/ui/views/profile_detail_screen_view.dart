import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/services/users_ranking_service.dart';
import 'package:gamification/ui/widgets/profile_picture.dart';
import 'package:gamification/utils/dimensions.dart';

import 'package:url_launcher/url_launcher.dart';

import '../../utils/app_colors.dart';
import '../router.dart';
import '../widgets/details_screens_canvas.dart';

class ProfileDetailScreenView extends StatelessWidget {
  String _message = ""; //User.message;
  double sizeText = 17;
  //Color primaryColor = Color.fromRGBO(58, 46, 150, 1);
  Color primaryColor = AppColors.attributeTextColor;
  @override
  Widget build(BuildContext context) {
    const String _appBarTitle = 'Perfil';
    const String _exitTitle = 'Sair';

    return Scaffold(
      backgroundColor: AppColors.backgroundLightBlue,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: AppColors.appBarIconColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text(
          _appBarTitle,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
        ),
        actions: <Widget>[
          FlatButton(
            textColor: AppColors.defaultGrey,
            //icon: Icon(Icons.exit_to_app),
            //tooltip: 'Sair do App',
            child: Text(
              _exitTitle,
              textAlign: TextAlign.end,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppColors.appBarIconColor),
            ),
            onPressed: () async {
              await FirebaseAuth.instance.signOut();
              Navigator.of(context).pushNamedAndRemoveUntil(
                  loginRoute, (Route<dynamic> route) => false);
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AvatarPicture(),
                  ],
                ),
              ),
              SizedBox(height: Dimensions.screenHeight(context) * 0.05),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Nome',
                          style:
                              TextStyle(color: Colors.grey, fontSize: sizeText),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          User.displayName,
                          style: TextStyle(
                              color: primaryColor, fontSize: sizeText),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Divider(),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Matricula',
                          style:
                              TextStyle(color: Colors.grey, fontSize: sizeText),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          User.registration,
                          style: TextStyle(
                              color: primaryColor, fontSize: sizeText),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Divider(),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'E-mail',
                          style:
                              TextStyle(color: Colors.grey, fontSize: sizeText),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          User.email,
                          style: TextStyle(
                              color: primaryColor, fontSize: sizeText),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Sobre",
                    style: TextStyle(color: Colors.grey, fontSize: sizeText),
                  ),
                ),
              ),
              TextFormField(
                maxLines: 5,
                maxLength: 150,
                style: TextStyle(color: primaryColor, fontSize: sizeText),
                //cursorColor: AppColors.blue,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Digite algo sobre você",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    enabledBorder: OutlineInputBorder(
                        //borderSide: BorderSide(color: AppColors.blue),
                        borderRadius: BorderRadius.circular(8.0)),
                    focusedBorder: OutlineInputBorder(
                        //borderSide: BorderSide(color: AppColors.blue),
                        borderRadius: BorderRadius.circular(8.0))),
                textInputAction: TextInputAction.next,
                onChanged: (value) {
                  print(value);
                  _message = value;
                  print(_message);
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return "_invalidFieldErrorMessage";
                  }
                  if (!(RegExp(r'^[a-zA-ZçÇ0-9]+$').hasMatch(value))) {
                    return "_invalidFieldErrorMessage";
                  }
                  //_message = value;
                  print(value);
                  return null;
                },
                //focusNode: focusNode,
                //validator: validator,
                //onFieldSubmitted: onFieldSubmitted,
                initialValue: User.message,
              ),
              MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                color: Colors.blue,
                onPressed: () async {
                  bool checkRequest =
                      await UsersRankingService.updateUserMessage(
                          userMessage: _message);
                  print(User.message);
                  if (checkRequest) {
                    User.message = _message;
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: new Text("Sucesso!"),
                          content:
                              new Text("Atualização efetuada com sucesso!"),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text("ok"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  } else {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: new Text("Erro!"),
                          content: new Text("Atualização não efetuada."),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text("ok"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }
                },
                child: Text("Salvar",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
