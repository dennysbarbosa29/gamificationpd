import 'package:flutter/material.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/historic_controller.dart';
import 'package:gamification/ui/widgets/details_card.dart';
import 'package:gamification/ui/widgets/details_screens_canvas.dart';
import 'package:gamification/ui/widgets/slidable_tile.dart';
import 'package:gamification/utils/icons.dart';
import 'package:provider/provider.dart';

class PurchaseHistoryScreenView extends StatelessWidget {
  const PurchaseHistoryScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String _appBarTitle = 'Histórico';
    return ChangeNotifierProvider(
      create: (_) => HistoricController(),
      child: Consumer<HistoricController>(
          builder: (context, historicController, _) {
        return TabedDetailsScreenCanvas(
          screenTitle: _appBarTitle, 
          tabs: [
          historicController.state == ViewState.Busy
              ? Center(child: CircularProgressIndicator())
              : DetailsScreenCanvas(
                  screenTitle: null,
                  listTitle: 'Compras',
                  // bodyWidget: RenderTiles(
                  //   content: {},
                  //   listContent: [],
                  //   tileType: TileType.Purchased,
                  // ),
                  bodyWidget: buildRewardsTiles(),
                  onRefresh: () async {
                    await historicController.update();
                  },
                ),
          historicController.state == ViewState.Busy
              ? Center(child: CircularProgressIndicator())
              : DetailsScreenCanvas(
                  screenTitle: null,
                  cardWidget: DetailsCard(
                    cardType: CardType.MissionHistory,
                    secondBalloonContent: historicController.missions.length,
                    thirdBalloonContent: User.totalPoints,
                  ),
                  listTitle: 'Missões Cumpridas',
                  bodyWidget: buildMissionsTiles(),
                  onRefresh: () async {
                    await historicController.update();
                  },
                  // bodyWidget: RenderTiles(
                  //   content: {},
                  //   listContent: [],
                  //   tileType: TileType.Mission,
                  // ),
                ),
        ]);
      }),
    );
  }

  Widget buildMissionsTiles() {
    return Consumer<HistoricController>(
        builder: (context, historicController, _) {
      return Column(
        children: historicController.missions
        .map((mission) => SlidableTile(
              tileTitle: mission.title,
              tileDescription: mission.subTitle,
              tileType: TileType.Mission,
              date: mission.date,
              reward: mission.points,
              enabled: false,
              icon: AppIcons.getIconDataFromIconName(mission.iconName),
            ))
        .toList(),
      );
    });
  }

  Widget buildRewardsTiles() {
    return Consumer<HistoricController>(
        builder: (context, historicController, _) {
      return Column(
        children: historicController.rewards
        .map((rewards) => SlidableTile(
              tileTitle: rewards.title,
              tileDescription: rewards.subTitle,
              tileType: TileType.Purchased,
              date: rewards.date,
              price: rewards.value,
              reward: rewards.points,
              enabled: false,
            ))
        .toList(),
      );
    });
  }
}
