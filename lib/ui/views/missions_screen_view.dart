import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/ui/widgets/details_card.dart';
import 'package:gamification/ui/widgets/details_screens_canvas.dart';
import 'package:gamification/ui/widgets/slidable_tile.dart';
import 'package:gamification/ui/widgets/slidable_tiles_renderer.dart';
import 'package:provider/provider.dart';

class MissionsScreenView extends StatelessWidget {
  const MissionsScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String _appBarTitle = '';
    return Consumer<HomeScreenController>(
      builder: (context, HomeScreenController homeScreenController, _) {
        // int currentProgress =
        //     (homeScreenController.selectedGame.mission.currentProgress * 100)
        //         .truncate();
        // int desiredProgress =
        //     (homeScreenController.selectedGame.mission.desiredProgress * 100)
        //         .truncate();
        // int goalOfTheDay =
        //     (homeScreenController.selectedGame.mission.goalOfTheDay * 100)
        //         .truncate();


        double currentProgress = User.performance;
        // int desiredProgress = 0;
        // int goalOfTheDay = 0;

        _appBarTitle = homeScreenController.selectedGame.name;

        return WillPopScope(
          onWillPop: () async {
            if (homeScreenController.state == ViewState.Error) return false;
            return true;
          },
          child: DetailsScreenCanvas(
            screenTitle: _appBarTitle,
            cardWidget: DetailsCard(
              cardType: CardType.Mission,
              firstBalloonContent: currentProgress,
              // secondBalloonContent: desiredProgress,
              // thirdBalloonContent: goalOfTheDay,
            ),
            listTitle: 'Missões',
            bodyWidget: homeScreenController.state == ViewState.Busy
            ? Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Center(child: CircularProgressIndicator()),
            )
            : homeScreenController.state == ViewState.Error
            ? Stack(
              children: <Widget>[ 
                GestureDetector(
                  onTap: homeScreenController.clearErrorState,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.6,
                    color: Colors.transparent,
                  ),
                ),
                AlertDialog(
                  title: Center(child: Text('Erro')),
                  content: Center(child:Text(homeScreenController.errorMessage)),
                  elevation: 3.0,
                ),
              ],
            )
            : RenderTiles(
              tileType: TileType.Mission,
              listContent: homeScreenController.selectedGame.missions,
              controller: homeScreenController,
            ),
            onRefresh: () async {
              homeScreenController.shouldReloadGames = true;
              await homeScreenController.loadGames();
            },
          ),
        );
      },
    );
  }
}
