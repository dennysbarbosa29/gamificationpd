import 'package:flutter/material.dart';
import 'package:gamification/models/mission_model.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/utils/app_colors.dart';
import 'package:provider/provider.dart';

class MissionDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String appBarTitle = 'Detalhes da missão';
    final double textSize = 17;
    Color primaryColor = AppColors.attributeTextColor;

    return Consumer<HomeScreenController>(
      builder: (BuildContext context, HomeScreenController controller, _) {
        Mission mission = controller.getMissionToShowDetails();
        return Scaffold(
          backgroundColor: AppColors.backgroundLightBlue,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: AppColors.appBarIconColor,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            centerTitle: true,
            title: Text(
              appBarTitle,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Nome',
                            style: TextStyle(color: Colors.grey, fontSize: textSize),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            mission.name,
                            style: TextStyle(color: primaryColor, fontSize: textSize),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Divider(),
                  SizedBox(
                    height: 7,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Detalhes',
                            style: TextStyle(color: Colors.grey, fontSize: textSize),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            mission.instructions,
                            style: TextStyle(color: primaryColor, fontSize: textSize),
                            textAlign: TextAlign.justify,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}