import 'package:flutter/material.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/models/users_ranking_model.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/users_ranking_controller.dart';
import 'package:gamification/ui/widgets/user_info_dialog.dart';
import 'package:gamification/utils/dimensions.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../utils/app_colors.dart';

class UsersRankingView extends StatelessWidget {
  final double sizeText = 17;
  final Color primaryColor = AppColors.attributeTextColor;
  @override
  Widget build(BuildContext context) {
    final String _appBarTitle = 'Ranking de Usuários';

    return Scaffold(
      backgroundColor: AppColors.backgroundLightBlue,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: AppColors.appBarIconColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text(
          _appBarTitle,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
        ),
      ),
      body: ChangeNotifierProvider<UsersRankingController>(
        create: (_) => UsersRankingController(),
        child: Consumer<UsersRankingController>(
          builder: (context, usersRankingController, _) {
            return usersRankingController.state == ViewState.Busy
                ? Center(child: CircularProgressIndicator())
                : usersRankingController.state == ViewState.Error
                    ? Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Erro ao carregar o ranking de usuários',
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              usersRankingController.errorMessage,
                              textAlign: TextAlign.center,
                            ),
                            GestureDetector(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child:
                                    Icon(Icons.replay, color: Colors.grey[600]),
                              ),
                              onTap: () {
                                usersRankingController.update();
                              },
                            )
                          ],
                        ),
                      )
                    : Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Card(
                              elevation: 1.5,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Ranking',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 27,
                                        color: Colors.grey[800],
                                      ),
                                    ),
                                    Dimensions.heightSpacer(
                                        Dimensions.screenHeight(context) *
                                            0.02),
                                    Center(
                                      child: baloonAndLabel(
                                          baloonContent:
                                              User.ranking.toString(),
                                          label: 'Meu Ranking',
                                          context: context),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          buildRankingList(),
                        ],
                      );
          },
        ),
      ),
    );
  }

  Widget baloonAndLabel(
      {@required String label,
      @required String baloonContent,
      Color contentColor,
      BuildContext context}) {
    return Column(
      children: <Widget>[
        Text(
          label,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            color: Colors.grey[800],
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.005),
        Container(
          width: Dimensions.screenWidth(context) / 4.2,
          height: Dimensions.screenWidth(context) / 7,
          decoration: BoxDecoration(
            borderRadius:
                BorderRadius.circular(Dimensions.screenWidth(context) / 50),
            color: Colors.grey[200],
          ),
          child: Center(
            child: FittedBox(
              fit: BoxFit.contain,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  '$baloonContent°',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey[800], fontSize: 25),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget buildRankingList() {
    return Consumer<UsersRankingController>(
      builder: (context, usersRankingController, _) {
        int placing = 0;
        return Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: usersRankingController.users.map((user) {
                placing++;
                return buildUserTile(user, placing, context);
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget buildUserTile(RankingUser user, int placing, BuildContext context) {
    Image userProfilePicture;
    if (user.photoUrl != "")
      userProfilePicture = Image.network(
        user.photoUrl,
        fit: BoxFit.fitWidth,
        width: 100,
        height: 100,
      );
    return ListTile(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return UserInfoDialog(
                message: user.message,
                name: user.name,
                profilePicture: userProfilePicture,
                registration: user.registration,
                placing: placing,
              );
            });
      },
      leading: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 40,
            child: Text(
              '$placing°',
              textAlign: TextAlign.start,
              style: user.registration == User.registration
                  ? TextStyle(color: Colors.blue)
                  : null,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            alignment: Alignment.center,
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50.0),
                border: Border.all(
                  color: user.registration == User.registration
                      ? Colors.blue
                      : Colors.grey[600],
                  width: 1,
                )),
            child: user.photoUrl != ""
                ? ClipOval(child: userProfilePicture,)
                : Icon(Icons.person),
          ),
        ],
      ),
      subtitle: Text(
        user.name,
        style: user.registration == User.registration
            ? TextStyle(color: Colors.blue)
            : null,
      ),
      title: Text('Pontos: ${user.points}'),
      trailing: placing <= 3
          ? FaIcon(
              FontAwesomeIcons.trophy,
              size: 20,
              color: placing == 1
                  ? Colors.yellow[600]
                  : placing == 2 ? Colors.blueGrey[200] : Colors.brown,
            )
          : null,
    );
  }
}
