import 'package:flutter/material.dart';
import 'package:gamification/ui/views/home_screen_view.dart';
import 'package:gamification/ui/views/login_screen_view.dart';
import 'package:gamification/ui/views/mission_details_screen_view.dart';
import 'package:gamification/ui/views/missions_screen_view.dart';
import 'package:gamification/ui/views/purchase_history_screen_view.dart';
import 'package:gamification/ui/views/recent_activity_screen_view.dart';
import 'package:gamification/ui/views/rewards_screen_view.dart';
import 'package:gamification/ui/views/splash_screen_view.dart';
import 'package:gamification/ui/views/users_ranking_view.dart';



import 'views/profile_detail_screen_view.dart';

const String homeRoute = '/';
const String loginRoute = 'login';
const String splashRoute = 'splash';
const String rewardsRoute = 'rewards';
const String recentActivityRoute = 'recentActivity';
const String purchaseHistoryRoute = 'purchaseHistoryRoute';
const String missionsScreenRoute = 'missionsScreenRoute';
const String perfilDetailRoute = 'perfilDetailRoute';
const String userRankingRoute = 'userRankingRoute';
const String missionDetailsRoute = 'missionDetailsRoute';


class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(builder: (_) => HomeScreenView());
      case loginRoute:
        return MaterialPageRoute(builder: (_) => LoginScreenView());
      case splashRoute:      
        return MaterialPageRoute(builder: (_) => SplashScreenView());
      case rewardsRoute:
        return MaterialPageRoute(builder: (_) => RewardsScreenView());
      case recentActivityRoute:
        return MaterialPageRoute(builder: (_) => RecentActivityScreenView());
      case purchaseHistoryRoute:
        return MaterialPageRoute(builder: (_) => PurchaseHistoryScreenView());
      case missionsScreenRoute:
        return MaterialPageRoute(builder: (_) => MissionsScreenView());
      case perfilDetailRoute:
        return MaterialPageRoute(builder: (_) => ProfileDetailScreenView());
      case userRankingRoute:
        return MaterialPageRoute(builder: (_) => UsersRankingView());
      case missionDetailsRoute:
        return MaterialPageRoute(builder: (_) => MissionDetailsScreen());

      default:
        return MaterialPageRoute(
          builder: (_) {
            return Scaffold(
              body: Center(
                child: Text('BUG: Rota não definida para ${settings.name}'),
              ),
            );
          },
        );
    }
  }
}
