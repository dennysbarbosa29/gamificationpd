import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gamification/utils/app_colors.dart';
import 'package:gamification/utils/dimensions.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DetailsScreenCanvas extends StatefulWidget {
  final String screenTitle;
  final Widget cardWidget;
  final Widget bodyWidget;
  final String listTitle;
  final Function onRefresh;

  const DetailsScreenCanvas({
    Key key,
    @required this.screenTitle,
    this.cardWidget,
    @required this.bodyWidget,
    @required this.listTitle,
    this.onRefresh
  }) : super(key: key);

  @override
  _DetailsScreenCanvasState createState() => _DetailsScreenCanvasState();
}

class _DetailsScreenCanvasState extends State<DetailsScreenCanvas> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    if (widget.onRefresh != null) await widget.onRefresh();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    setState(() {
      _refreshController.loadComplete();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundLightBlue,
      appBar: widget.screenTitle == null
          ? null
          : AppBar(
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: AppColors.appBarIconColor,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              centerTitle: true,
              title: Text(
                this.widget.screenTitle,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
            ),
      body: SmartRefresher(
        enablePullDown: true,
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        child: ListView(
          //mainAxisAlignment: MainAxisAlignment.center,
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: this.widget.cardWidget,
            ),
            Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.02),
            Padding(
              padding: const EdgeInsets.only(left: 20, bottom: 10),
              child: Text(
                widget.listTitle,
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.grey[800],
                ),
                textAlign: TextAlign.left,
              ),
            ),
            Divider(
              color: Colors.grey[400],
              thickness: 1,
              indent: 10,
              endIndent: 10,
              height: 0,
            ),
            this.widget.bodyWidget,
          ],
        ),
      ),
    );
  }
}

class TabedDetailsScreenCanvas extends StatelessWidget {
  final String screenTitle;
  final List<Widget> tabs;

  const TabedDetailsScreenCanvas({
    Key key,
    @required this.screenTitle,
    @required this.tabs,
  });

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        backgroundColor: AppColors.backgroundLightBlue,
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: AppColors.appBarIconColor,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            centerTitle: true,
            title: Text(
              this.screenTitle,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
            bottom: TabBar(
              unselectedLabelColor: AppColors.appBarIconColor,
              labelColor: AppColors.appBarIconColor,
              tabs: [
                Tab(
                  text: "Recompensas",
                  icon: Icon(
                    Icons.card_giftcard,
                    color: AppColors.appBarIconColor,
                  )
                ),
                Tab(
                  text: "Missões",
                  icon: Icon(
                    Icons.videogame_asset,
                    color: AppColors.appBarIconColor,
                  )
                ),
              ],
            )),
        body: TabBarView(children: tabs),
      ),
    );
  }
}
