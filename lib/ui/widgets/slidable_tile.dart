import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:gamification/utils/app_colors.dart';
import 'package:gamification/utils/dimensions.dart';
import 'package:intl/intl.dart';

enum TileType { Challenge, Purchased, Rewards, Mission }

//
//https://flutterawesome.com/a-flutter-implementation-of-slidable-list-item-with-directional-slide-actions/

class SlidableTile extends StatelessWidget {
  final String tileTitle;
  final String tileDescription;
  final TileType tileType;
  final DateTime date;
  final int currentRedemptions;
  final int maxRedemptions;
  final String price;
  final IconData icon;
  final Color iconColor;
  final String reward;
  final Function onTapFunction;
  final bool active;
  final bool enabled;
  final bool canBuy; // Used in the reward tile
  final Function infoOnTapFunction;

  SlidableTile({
    Key key,
    @required this.tileTitle,
    @required this.tileDescription,
    @required this.tileType,
    this.date,
    this.currentRedemptions = 0,
    this.maxRedemptions = 0,
    this.price,
    this.icon,
    this.iconColor,
    this.reward,
    this.onTapFunction,
    this.active = false,
    this.canBuy = false,
    this.enabled = true,
    this.infoOnTapFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Slidable(
          enabled: enabled,
          actionPane: SlidableDrawerActionPane(),
          child: SlidableChild(
              icon: icon,
              iconColor: iconColor,
              tileTitle: tileTitle,
              tileType: tileType,
              price: price,
              tileDescription: tileDescription,
              date: date,
              currentRedemptions: currentRedemptions,
              maxRedemptions: maxRedemptions,
              reward: reward,
              active: active),
          secondaryActions: <Widget>[
            if (tileType == TileType.Mission && !active)
              IconSlideAction(
                foregroundColor: Colors.white,
                caption: 'Entrar',
                color: Colors.green[300],
                icon: Icons.input,
                onTap: onTapFunction ?? () {},
              ),
            if (tileType == TileType.Mission)
              IconSlideAction(
                foregroundColor: Colors.white,
                caption: 'Info',
                color: Colors.grey,
                icon: Icons.info_outline,
                onTap: infoOnTapFunction ?? () {},
              ),
            if (tileType == TileType.Rewards && canBuy)
              IconSlideAction(
                foregroundColor: Colors.white,
                caption: 'Solicitar',
                color: Colors.green[300],
                icon: Icons.check_circle,
                onTap: onTapFunction ?? () {},
              ),
            if (tileType == TileType.Rewards && !canBuy)
              IconSlideAction(
                foregroundColor: Colors.white,
                caption: 'Sem pontos',
                color: Colors.red[400],
                icon: Icons.error,
                onTap: () {},
              ),
          ],
          // actions: <Widget>[
          //   IconSlideAction(
          //     foregroundColor: Colors.white,
          //     caption: 'pipipi',
          //     color: Colors.grey,
          //     icon: Icons.info_outline,
          //     onTap: () {},
          //   ),
          //   IconSlideAction(
          //     foregroundColor: Colors.white,
          //     caption: 'popopo',
          //     color: Colors.grey,
          //     icon: Icons.delete,
          //     onTap: () {},
          //   ),
          // ],
        ),
        Divider(
          color: Colors.grey[400],
          thickness: 1,
          indent: 10,
          endIndent: 10,
          height: 0,
        )
      ],
    );
  }
}

class SlidableChild extends StatelessWidget {
  const SlidableChild({
    Key key,
    @required this.icon,
    @required this.iconColor,
    @required this.tileTitle,
    @required this.tileType,
    @required this.price,
    @required this.tileDescription,
    @required this.date,
    @required this.currentRedemptions,
    @required this.maxRedemptions,
    @required this.reward,
    @required this.active,
  }) : super(key: key);

  final IconData icon;
  final Color iconColor;
  final String tileTitle;
  final TileType tileType;
  final String price;
  final String tileDescription;
  final DateTime date;
  final int currentRedemptions;
  final int maxRedemptions;
  final String reward;
  final bool active;

  @override
  Widget build(BuildContext context) {
    bool isOpen;
    return GestureDetector(
      onTap: () {
        if (isOpen == null) isOpen = false;
        if (!isOpen) {
          Slidable.of(context).open(actionType: SlideActionType.secondary);
        } else {
          Slidable.of(context).close();
        }
        isOpen = !isOpen;
      },
      child: Container(
        height: 75,
        width: Dimensions.screenWidth(context),
        color: AppColors.backgroundLightBlue,
        child: ListTile(
          leading: Icon(
            icon ?? Icons.card_giftcard,
            size: 35,
            color: iconColor != null ? iconColor : Colors.grey[800],
          ),
          title: Text(
            tileType == TileType.Rewards || tileType == TileType.Purchased
                ? '$tileTitle R\$$price'
                : tileTitle,
            style: TextStyle(
                color: Colors.grey[800],
                fontWeight: FontWeight.bold,
                fontSize: 16),
          ),
          subtitle: Text(
            tileDescription,
            style: TextStyle(
              color: Colors.grey[700],
              fontSize: 14,
            ),
          ),
          trailing: FittedBox(
            fit: BoxFit.fitWidth,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        tileType == TileType.Purchased ||
                                tileType == TileType.Mission
                            ? DateFormat('dd/MM')
                                .format(date.toLocal())
                                .toString()
                            : currentRedemptions.toString() +
                                '/' +
                                maxRedemptions.toString(),
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[700],
                        ),
                      ),
                      Dimensions.heightSpacer(5),
                      Text(
                        tileType == TileType.Mission ||
                                tileType == TileType.Rewards ||
                                tileType == TileType.Purchased
                            ? reward + ' pontos'
                            : price, //Formatters().intWithDot(price) + ' points',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[700],
                        ),
                      )
                    ],
                  ),
                  if (active)
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.check_circle,
                        size: 20,
                        color: Colors.green[300],
                      ),
                    ),
                ]),
          ),
        ),
      ),
    );
  }
}
