import 'dart:io';

import 'package:flutter/material.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:provider/provider.dart';

import '../controllers/profile_picture_controller.dart';
import 'change_profile_picture_dialog.dart';

final double imageSizeProfileDetails = 100.0;
final double imageSizeHome = 60;

// To be shown when there is no profile picture set
final Widget defaultProfilePictureWidget = Container(
  decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(50.0),
    border: Border.all(
      color: Colors.grey[600],
      width: 1,
    ),
  ),
  child: ClipOval(
    child: Image.asset(
      'assets/images/avatar1.png',
      fit: BoxFit.fill,
      height: imageSizeProfileDetails,
      width: imageSizeProfileDetails,
    ),
  ),
);

class AvatarPicture extends StatefulWidget {
  @override
  _AvatarPictureState createState() => _AvatarPictureState();
}

class _AvatarPictureState extends State<AvatarPicture> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return ChangeProfilePictureDialog(
                icon: IconData(58294, fontFamily: 'MaterialIcons'),
                barColor: Colors.blue[800],
                textColor: Colors.grey[700],
                title: 'Alterar foto de perfil',
                body: '',
                buttonText: ['Camera', 'Galeria']);
          },
        );
      },
      child: Consumer<ProfilePictureController>(
        builder:
            (BuildContext context, ProfilePictureController controller, _) {
          return Container(
              height: imageSizeProfileDetails,
              width: imageSizeProfileDetails,
              child: Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      border: Border.all(
                        color: Colors.grey[600],
                        width: 1,
                      ),
                    ),
                    height: imageSizeProfileDetails,
                    width: imageSizeProfileDetails,
                    child: controller.state == ViewState.Idle
                        ? controller.hasImage
                            ? ClipOval(
                                child: Image.file(controller.profilePictureFile,
                                    fit: BoxFit.fitWidth,
                                    height: imageSizeProfileDetails,
                                    width: imageSizeProfileDetails),
                              )
                            : defaultProfilePictureWidget
                        : controller.state == ViewState.Error
                            ? defaultProfilePictureWidget
                            : ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxHeight: imageSizeProfileDetails - 20,
                                    maxWidth: imageSizeProfileDetails - 20),
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                  ),
                  Container(
                    alignment: Alignment.bottomRight,
                    height: imageSizeProfileDetails + 2,
                    width: imageSizeProfileDetails,
                    child: Container(
                      padding: EdgeInsets.all(3),
                      child: Icon(
                        Icons.edit,
                        color: Colors.white,
                        size: 15,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ],
              ));
        },
      ),
    );
  }
}

class AvatarPictureShow extends StatefulWidget {
  @override
  _AvatarPictureShowState createState() => _AvatarPictureShowState();
}

class _AvatarPictureShowState extends State<AvatarPictureShow> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ProfilePictureController>(
      builder: (BuildContext context, ProfilePictureController controller, _) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0),
            border: Border.all(
              color: Colors.grey[600],
              width: 1,
            ),
          ),
          height: imageSizeHome,
          width: imageSizeHome,
          child: controller.state == ViewState.Idle
              ? controller.hasImage
                  ? ClipOval(
                      child: Image.file(controller.profilePictureFile,
                          fit: BoxFit.fitWidth,
                          height: imageSizeHome,
                          width: imageSizeHome),
                    )
                  : defaultProfilePictureWidget
              : controller.state == ViewState.Error
                  ? defaultProfilePictureWidget
                  : ConstrainedBox(
                      constraints: BoxConstraints(
                          maxHeight: imageSizeHome - 20,
                          maxWidth: imageSizeHome - 20),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
        );
      },
    );
  }
}
