import 'package:flutter/material.dart';

class LoginSingUpBackground extends StatefulWidget {
  final Widget child;
  const LoginSingUpBackground({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  _LoginSingUpBackgoundState createState() => _LoginSingUpBackgoundState();
}

class _LoginSingUpBackgoundState extends State<LoginSingUpBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // makes the container fill the whole screen
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Theme.of(context).primaryColor,
            Theme.of(context).primaryColorLight
          ],
        ),
      ),
      child: widget.child,
    );
  }
}
