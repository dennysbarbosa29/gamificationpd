import 'package:flutter/material.dart';

class LoginSingUpField extends StatefulWidget {
  final bool isPasswordField;
  final Color backgroundColor;
  final IconData icon;
  final String hintText;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final String Function(String) validator;
  final String Function(String) onSaved;
  final void Function(String) onFieldSubmitted;
  final String Function(String, String) validator2;
  final TextStyle style;
  final String initValue;
  LoginSingUpField({
    Key key,
    this.isPasswordField = false,
    this.backgroundColor = Colors.white,
    this.icon,
    @required this.hintText,
    @required this.focusNode,
    this.keyboardType,
    this.textInputAction = TextInputAction.next,
    this.validator,
    this.onSaved,
    this.style,
    this.onFieldSubmitted,
    this.validator2,
    this. initValue
  }) : super(key: key);

  @override
  _LoginSingUpFieldState createState() => _LoginSingUpFieldState();
}

class _LoginSingUpFieldState extends State<LoginSingUpField> {
  bool _obscureText;
  @override
  void initState() {
    widget.isPasswordField ? _obscureText = true : _obscureText = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        //prefixIcon: Icon(widget.icon, size: Theme.of(context).iconTheme.size),
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: widget.hintText,
        fillColor: widget.backgroundColor,
        filled: true,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
        errorStyle: TextStyle(
          fontSize: 11.0
        ),

        // suffixIcon: widget.isPasswordField
        //     ? Padding(
        //         padding: EdgeInsetsDirectional.only(end: 8),
        //         child: IconButton(
        //           icon: Icon(
        //             // Based on passwordVisible state choose the icon
        //             _obscureText ? Icons.visibility : Icons.visibility_off,
        //             color: Theme.of(context).primaryColorDark,
        //             size: Theme.of(context).iconTheme.size,
        //           ),
        //           onPressed: () => setState(
        //             () {
        //               _obscureText ^= true; //toggles bool value
        //             },
        //           ),
        //         ),
        //       )
        //     : SizedBox(
        //         width: 8,
        //       ),
      ),
      obscureText: _obscureText,
      textAlign: TextAlign.center,
      keyboardType: widget.keyboardType,
      textInputAction: widget.textInputAction,
      validator: widget.validator,
      onSaved: widget.onSaved,
      focusNode: widget.focusNode,
      style: widget.style,
      onFieldSubmitted: widget.onFieldSubmitted,
      initialValue: widget.initValue,
    );
  }
}
