import 'package:flutter/material.dart';
import 'package:gamification/utils/dimensions.dart';
import 'package:gamification/utils/formatters.dart';

enum CardType { Points, Mission, MissionHistory }

class DetailsCard extends StatelessWidget {
  final CardType cardType;
  final firstBalloonContent;
  final secondBalloonContent;
  final thirdBalloonContent;
  const DetailsCard(
      {Key key,
      this.cardType,
      this.firstBalloonContent,
      this.secondBalloonContent,
      this.thirdBalloonContent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color baloonColor = Colors.grey[200];
    String cardTitle;
    String label1;
    String label2;
    String label3;
    if (cardType == CardType.Points) {
      cardTitle = 'Pontuação';
      label1 = 'Ranking';
      label2 = 'Meus créditos';
      label3 = '';
    } else {
      if (cardType == CardType.Mission) {
        cardTitle = 'Missão';
        label1 = 'Você';
        label2 = 'Agora';
        label3 = 'Objetivo';
      }
      if (cardType == CardType.MissionHistory) {
        cardTitle = 'Detalhes';
        label1 = 'Início';
        label2 = 'Medalhas';
        label3 = 'Pontos Totais';
      }
    }

    Widget baloonAndLabel({
      @required String label,
      @required String baloonContent,
      Color contentColor,
    }) {
      return Column(
        children: <Widget>[
          Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              color: Colors.grey[800],
            ),
          ),
          Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.005),
          Container(
            width: Dimensions.screenWidth(context) / 4.2,
            height: Dimensions.screenWidth(context) / 7,
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(Dimensions.screenWidth(context) / 50),
              color: baloonColor,
            ),
            child: Center(
              child: FittedBox(
                fit: BoxFit.contain,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    baloonContent,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: contentColor != null
                            ? contentColor
                            : Colors.grey[800],
                        fontSize: 25),
                  ),
                ),
              ),
            ),
          )
        ],
      );
    }

    return Card(
      elevation: Dimensions.cardElevation - 3.5,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  cardTitle,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 27,
                    color: Colors.grey[800],
                  ),
                ),
                cardType == CardType.Mission
                    ? GestureDetector(
                        onTap: () {
                          print('tapped');
                        },
                        child: Container(
                          child: Icon(
                            Icons.info_outline,
                            color: Colors.grey[700],
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
            Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.03),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if (firstBalloonContent != null)
                  baloonAndLabel(
                      baloonContent: cardType == CardType.Mission
                          ? firstBalloonContent.toString() + '%'
                          : firstBalloonContent.toString().substring(0, 10),
                      label: label1),
                if (secondBalloonContent != null)
                  baloonAndLabel(
                      baloonContent: cardType == CardType.Mission
                          ? secondBalloonContent.toString() + '%'
                          : Formatters().intWithDot(secondBalloonContent),
                      label: label2,
                      contentColor: cardType == CardType.Mission
                          ? (secondBalloonContent < thirdBalloonContent
                              ? Colors.red
                              : Colors.green)
                          : null),
                if (thirdBalloonContent != null)
                  baloonAndLabel(
                      baloonContent: cardType == CardType.Mission
                          ? thirdBalloonContent.toString() + '%'
                          : Formatters().intWithDot(thirdBalloonContent),
                      label: label3),
              ],
            )
          ],
        ),
      ),
    );
  }
}
