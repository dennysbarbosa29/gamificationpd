import 'package:flutter/material.dart';

class AppListTile extends StatelessWidget {
  final IconData leadingIcon;
  final String text;
  final String routeName;
  final double horizontalPadding;
  const AppListTile({
    Key key,
    @required this.leadingIcon,
    @required this.text,
    @required this.routeName,
    this.horizontalPadding = 0.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: ListTile(
        leading: Icon(
          leadingIcon,
          color: Colors.black,
        ),
        title: Text(
          text,
          style:
              Theme.of(context).textTheme.subhead.copyWith(color: Colors.black),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Colors.black,
        ),
        onTap: () => Navigator.of(context).pushNamed(routeName),
      ),
    );
  }
}
