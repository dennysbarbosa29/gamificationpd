import 'package:flutter/material.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/ui/controllers/base_controller.dart';
import 'package:gamification/ui/controllers/home_screen_controller.dart';
import 'package:gamification/utils/icons.dart';

import '../router.dart';
import 'slidable_tile.dart';

class RenderTiles extends StatelessWidget {
  final Map<dynamic, dynamic> content;
  final List<dynamic> listContent;
  final TileType tileType;
  final BaseController controller;

  RenderTiles({
    Key key,
    this.content,
    this. listContent,
    @required this.tileType,
    this.controller,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if ((content != null && content.isNotEmpty) || (listContent != null && listContent.isNotEmpty)) {
      List<Widget> slidables = [];

      switch (tileType) {
        case TileType.Rewards:
          {
            content.forEach((mapName, tileItem) {
              if (int.parse(tileItem.used) < int.parse(tileItem.limit)) {
                slidables.add(SlidableTile(
                  tileType: tileType,
                  tileTitle: tileItem.title,
                  tileDescription: tileItem.subTitle,
                  reward: tileItem.points,
                  price: tileItem.value,
                  maxRedemptions: int.parse(tileItem.limit),
                  currentRedemptions: int.parse(tileItem.used),
                  iconColor: Color(tileItem.color),
                  canBuy: User.points >= int.parse(tileItem.points),
                  onTapFunction: () {
                    (controller as HomeScreenController).requestReward(tileItem.id);
                  }
                  // tileItem.rewardStatus == RewardStatus.canBuy
                  //     ? Colors.green
                  //     : tileItem.rewardStatus == RewardStatus.notEnoughtPoints
                  //         ? Colors.blue
                  //         : null,
                ));
              }
            });
          }
          break;

        case TileType.Purchased:
          {
            content.forEach((mapName, tileItem) {
              slidables.add(SlidableTile(
                price: tileItem.price,
                tileTitle: tileItem.name,
                tileDescription: tileItem.description,
                tileType: tileType,
                date: tileItem.date,
              ));
            });
          }
          break;

        case TileType.Challenge:
          {
            content.forEach((mapName, tileItem) {
              slidables.add(SlidableTile(
                price: tileItem.price,
                tileTitle: tileItem.name,
                tileDescription: tileItem.description,
                tileType: tileType,
                maxRedemptions: tileItem.challengeGoal,
                currentRedemptions: tileItem.challengeProgress,
              ));
            });
          }
          break;

        case TileType.Mission:
          {
            listContent.forEach((tileItem) {
              slidables.add(SlidableTile(
                tileType: tileType,
                tileTitle: tileItem.name,
                tileDescription: tileItem.description,
                icon: AppIcons.getIconDataFromIconName(tileItem.iconName),
                iconColor: Color(tileItem.color),
                date: tileItem.endDate,
                reward: tileItem.reward,
                active: tileItem.userIsParticipating,
                onTapFunction: () {
                  (controller as HomeScreenController).setUserParticipatingInMission(tileItem.id);
                },
                infoOnTapFunction: () {
                  (controller as HomeScreenController).missionToShowDetailsId = tileItem.id;
                  Navigator.pushNamed(context, missionDetailsRoute);
                },
              ));
            });
          }
          break;
      }

      return Column(
        children: slidables,
      );
    } else
      return Container(
        padding: EdgeInsets.only(top: 20),
        child: Text(
          'Nenhum item para ser exibido',
          textAlign: TextAlign.center,
        ),
      );
  }
}
