import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/utils/hex_color.dart';


class UserInfoDialog extends StatelessWidget {
  final String name;
  final String registration;
  final Image profilePicture;
  final String message;
  final int placing;

  UserInfoDialog({
    @required this.name,
    @required this.registration,
    @required this.profilePicture,
    @required this.message,
    @required this.placing,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: HexColor('e9f2f2'),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Container(
        margin: EdgeInsets.all(15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            buildUserInfos(),
            SizedBox(
              height: 20,
            ),
            buildMessageText(),
          ],
        ),
      ),
    );
  }
  
  Widget buildUserInfos() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 90,
          height: 90,
          child: Stack(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(50.0),
                  border: Border.all(
                    color: registration == User.registration
                        ? Colors.blue
                        : Colors.grey[600],
                    width: 1,
                  ),
                ),
                child: profilePicture != null
                    ? ClipOval(child: profilePicture)
                    : Icon(
                        Icons.person,
                        size: 60,
                        color: Colors.grey,
                      ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                child: placing <= 3
                    ? FaIcon(
                        FontAwesomeIcons.trophy,
                        size: 22,
                        color: placing == 1
                            ? Colors.yellow[600]
                            : placing == 2
                                ? Colors.blueGrey[200]
                                : Colors.brown,
                      )
                    : null,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              name,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Matrícula: $registration',
              style: TextStyle(
                fontSize: 15,
              ),
            )
          ],
        )
      ],
    );
  }

  Widget buildMessageText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'Saudação',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            letterSpacing: 0.5,
            color: Colors.grey[600],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
              color: Colors.lightBlue[200],
              width: 1,
            ),
          ),
          height: 100,
          child: Text(
            message != '' ? message : 'Parece que ' + name.split(' ')[0] + ' não possui algo a dizer por enquanto.',
            textAlign: TextAlign.justify,
          ),
        )
      ],
    );
  }
}
