import 'dart:io';
import 'package:flutter/services.dart';
import 'package:gamification/services/profile_picture_service.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';

import 'base_controller.dart';

class ProfilePictureController extends BaseController {
  static const String PROFILE_PICTURE_FILENAME = 'profile_picture.png';
  String _documentsDirectory = '';
  String _profilePicturePath = '';
  File _profilePictureFile;
  String _uid = '';
  String _imageUrl = '';

  bool _hasImage = false;

  void setUid(String uid) {
    _uid = uid;
    init();
  }

  File get profilePictureFile => _profilePictureFile;
  bool get hasImage => _hasImage;
  String get imageUrl => _imageUrl;

  Future<void> init() async {
    this.setState(ViewState.Busy);
    _documentsDirectory = (await getApplicationDocumentsDirectory()).path;
    _profilePicturePath = join(
      _documentsDirectory,
      PROFILE_PICTURE_FILENAME,
    );

    // // Open image if profile picture path points to something
    // if (FileSystemEntity.typeSync(_profilePicturePath) != FileSystemEntityType.notFound) {
    //   _profilePictureFile = File(_profilePicturePath);
    //   _hasImage = true;
    // } else {
    //   _hasImage = false;
    // }

    try {
      // Get image URL
      _imageUrl = await ProfilePictureService.getProfilePictureURL(_uid);
    } on PlatformException {
      _hasImage = false;
      this.setState(ViewState.Idle);
      notifyListeners();
      return;
    }

    // Download image
    try {
      _profilePictureFile = await ProfilePictureService.downloadProfilePictureToPath(_uid, _profilePicturePath);
      _hasImage = true;
      this.setState(ViewState.Idle);
      notifyListeners();
    } on PlatformException catch (e) {
      this.setState(ViewState.Error);
      this.setErrorMessage('Failed to download profile picture. ${e.toString()}');
      notifyListeners();
    }
  }

  Future<void> getImageFromSource(ImageSource source) async {
    this.setState(ViewState.Busy);

    // Get file from image picker
    File newProfilePicture = await ImagePicker.pickImage(source: source);

    // Upload file to Firebase
    if (newProfilePicture != null) {
      _profilePictureFile = newProfilePicture;
      try {
        await ProfilePictureService.uploadProfilePicture(_uid, _profilePictureFile);
      } on PlatformException catch (e) {
        this.setState(ViewState.Error);
        this.setErrorMessage('Failed to upload profile picture. ${e.toString()}');
        notifyListeners();
        return;
      }

      try {
        String url = await ProfilePictureService.getProfilePictureURL(_uid);
        await ProfilePictureService.saveProfilePictureUrl(_uid, url);
      } catch(e) {
        _hasImage = true;
        this.setState(ViewState.Error);
        this.setErrorMessage('Failed to save profile picture path. ${e.toString()}');
        print(this.errorMessage);
        notifyListeners();
      }

      // Copy the temp file to the permanent file path
      // if (_profilePictureFile != null) {
      //   await _profilePictureFile.copy('$_profilePicturePath');
      // }

      _hasImage = true;
      this.setState(ViewState.Idle);
      notifyListeners();
    } else {
      this.setState(ViewState.Idle);
      notifyListeners();
    }
  }
}
