import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:gamification/utils/store_urls.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:package_info/package_info.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

class VersionCheck extends ChangeNotifier {
  double _currentVersion;
  double _askVersion;
  double _forceVersion;
  bool _mustShowDialog = false;
  bool _forceAsk;
  bool isIos;

  VersionCheck(){
    isIos = Platform.isIOS;
    getVersions();    
  }

  Future<void> launchURL() async {
    if (await canLaunch(isIos ? StoreUrl.playStoreUrl : StoreUrl.appStoreUrl)) {
      await launch(isIos ? StoreUrl.playStoreUrl : StoreUrl.appStoreUrl);
    } else {
      throw 'Could not launch $isIos ? StoreUrl.playStoreUrl : StoreUrl.appStoreUrl';
    }    
  }

  bool get getShowDialog => _mustShowDialog;
  bool get getForceAsk => _forceAsk;

  Future<void> getVersions() async {
    //Get the device current installed version
    final PackageInfo info = await PackageInfo.fromPlatform();
    _currentVersion = double.parse(info.version.trim().replaceAll(".", ""));
    print('Current Version: $_currentVersion');

    //Get Latest version info from firebase config
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    final RemoteConfigSettings configSettings = RemoteConfigSettings(
      debugMode: true,
    ); //only while developing. remove later. allows fetching several times
    remoteConfig.setConfigSettings(configSettings);

    try {
      // Using default duration to force fetching from remote server.
      await remoteConfig.fetch(expiration: const Duration(seconds: 600));
      await remoteConfig.activateFetched();
      remoteConfig.getString('force_update_current_version');
      print(remoteConfig.getString('force_update_current_version'));

      _forceVersion = double.parse(remoteConfig
          .getString('force_update_current_version')
          .trim()
          .replaceAll(".", ""));
      print('Force Version Parsed: $_forceVersion');

      _askVersion = double.parse(remoteConfig
          .getString('ask_update_current_version')
          .trim()
          .replaceAll(".", ""));
      print('Ask Version Parsed: $_askVersion');

      if (_forceVersion > _currentVersion) {
      _mustShowDialog = true;
      _forceAsk = false; //force update
    } else {
      if (_askVersion > _currentVersion) {
        _mustShowDialog = true;
        _forceAsk =  true; //ask for update
      } else _mustShowDialog = false;
    }
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print(exception);
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    
  }
}
