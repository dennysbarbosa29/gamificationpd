
import 'package:gamification/models/users_ranking_model.dart';
import 'package:gamification/repository/users_ranking_repository.dart';

import 'base_controller.dart';

class UsersRankingController extends BaseController {
  UsersRankingRepository _usersRankingRepository = UsersRankingRepository();

  UsersRankingController() {
    update();
  }

  List<RankingUser> get users => _usersRankingRepository.users;

  void update() async {
    this.setState(ViewState.Busy);
    try {
      await _usersRankingRepository.getUsers();
      this.setState(ViewState.Idle);
    } catch (exception) {
      this.setErrorMessage(exception.toString());
      this.setState(ViewState.Error);
    }
  }
}
