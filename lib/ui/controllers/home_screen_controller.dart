
import 'package:gamification/models/game_model.dart';
import 'package:gamification/models/mission_model.dart';
import 'package:gamification/models/reward_model.dart';
import 'package:gamification/models/user_model.dart';
import 'package:gamification/services/game_fetcher.dart';
import 'package:gamification/services/mission_service.dart';
import 'package:gamification/services/reward_service.dart';

import 'base_controller.dart';

class FetchGamesResult {
  bool status;
  String errorCode;
  String errorMessage;
}

class HomeScreenController extends BaseController {
  HomeScreenController();

  NetworkGameFetcherAdapter _gameFetcher = NetworkGameFetcherAdapter();
  List<Game> _gamesList;
  Game _selectedGame;
  bool _areGamesLoaded = false;
  bool _shouldReloadGames = false;
  String _missionToShowDetailsId = '';

  int get numberOfGames => _gamesList.length;
  bool get areGamesLoaded => _areGamesLoaded;

  set shouldReloadGames(bool flag) => _shouldReloadGames = flag;
  set missionToShowDetailsId(String id) => _missionToShowDetailsId = id;

  Future<void> loadGames() async {
    if (!_areGamesLoaded || _shouldReloadGames) {
      this.setState(ViewState.Busy);
      updateGamesList(_shouldReloadGames).then((result) {
        if (result.status) {
          setSelectedGame(0);
          this.setState(ViewState.Idle);
        } else {
          this.setState(ViewState.Error);
          this.setErrorMessage(result.errorMessage);
        }
      });
      _areGamesLoaded = true;
      _shouldReloadGames = false;
    }
  }

  Future<FetchGamesResult> updateGamesList(bool reload) async {
    FetchGamesResult result = FetchGamesResult();
    _gameFetcher.setUid(User.uid);
    try {
      _gameFetcher.reload = reload;
      _gamesList = await _gameFetcher.getGamesList();
      result.status = true;
      return result;
    } catch (exception) {
      result.status = false;
      result.errorCode = 'ERROR_LOADING_GAMES';
      result.errorMessage = exception.toString();
      return result;
    }
  }

  void setSelectedGame(int index) {
    _selectedGame = _gamesList[index];
    notifyListeners();
    //DEBUG -> DELETE THIS
    int iterator = 0;
    _selectedGame.rewardsMap.forEach((keys, value) {
      print('$iterator - ' + value.toString());
      iterator++;
    });
    print(_selectedGame.purchaseHistory);
    print('-------------------------------');
    //_selectedGame.getSortedPurchaseList();
    //EOD
  }

  Game get selectedGame => _selectedGame;

  Future<void> setUserParticipatingInMission(String missionId) async {
    this.setState(ViewState.Busy);
    notifyListeners();
    
    MissionServiceResult result = await MissionService.setUserParticipatingInMission(uid: User.uid, missionId: missionId);
    if (result.status) {
      for(Mission mission in selectedGame.missions) {
        if (mission.id == missionId) {
          mission.setUserIsParticipating(true);
          notifyListeners();
        }
      }
      this.setState(ViewState.Idle);
      notifyListeners();
    } else {
      this.setState(ViewState.Error);
      this.setErrorMessage(result.errorMessage);
      notifyListeners();
    }
  }

  void clearErrorState() {
    if(state == ViewState.Error) {
      this.setState(ViewState.Idle);
      this.setErrorMessage('');
    }
    notifyListeners();
  }

  Future<void> requestReward(String rewardId) async {
    this.setState(ViewState.Busy);
    notifyListeners();
    
    RewardServiceResult result = await RewardService.requestReward(uid: User.uid, rewardId: rewardId);
    if (result.status) {
      // Update the number of times that the reward was requested and the user's points
      for(Reward reward in selectedGame.rewardsMap.values) {
        if (reward.id == rewardId) {
          reward.incrementUsedTimes();
          User.points = User.points - int.parse(reward.points);
          notifyListeners();
          break;
        }
      }
      this.setState(ViewState.Idle);
      notifyListeners();
    } else {
      this.setState(ViewState.Error);
      this.setErrorMessage(result.errorMessage);
      notifyListeners();
    }
  }

  Mission getMissionToShowDetails({String id}) {
    String idToSearch = id == null ? _missionToShowDetailsId : id;
    for(Mission mission in selectedGame.missions) {
      if (mission.id == idToSearch) {
        return mission;
      }
    }
  }
}

///Legacy Mock
/* List<Game> gameListMock = [
  Game(
    name: 'GameLabs',
    ranking: 2,
    currentRewardPoints: 3993,
    totalRewardPoints: 5143,
    mission: Mission(
      currentProgress: 0.05,
      desiredProgress: 0.1,
      goalOfTheDay: 0.2,
    ),
  ),
  Game(
    name: 'PokerTech',
    ranking: 4,
    currentRewardPoints: 455,
    totalRewardPoints: 2898,
    mission: Mission(
      currentProgress: 0.4,
      desiredProgress: 0.46,
      goalOfTheDay: 0.57,
    ),
  ),
  Game(
    name: 'Game3',
    ranking: 17,
    currentRewardPoints: 200,
    totalRewardPoints: 200,
    mission: Mission(
      currentProgress: 0.67,
      desiredProgress: 0.75,
      goalOfTheDay: 0.9,
    ),
  ),
  Game(
    name: 'Game44',
    ranking: 100,
    currentRewardPoints: 0,
    totalRewardPoints: 100,
    mission: Mission(
      currentProgress: 0.0,
      desiredProgress: 0.1,
      goalOfTheDay: 0.25,
    ),
  ),
  Game(
    name: 'Game555',
    ranking: 4,
    currentRewardPoints: 2100,
    totalRewardPoints: 6200,
    mission: Mission(
      currentProgress: 0.9,
      desiredProgress: 1,
      goalOfTheDay: 1,
    ),
  ),
]; */
