

import 'package:gamification/models/mission_accomplished_model.dart';
import 'package:gamification/models/purchased_reward_model.dart';
import 'package:gamification/repository/historic_repository.dart';

import 'base_controller.dart';

class HistoricController extends BaseController {
  HistoricRepository _historicRepository = HistoricRepository();

  HistoricController() {
    update();
  }

  List<PurchasedReward> get rewards => _historicRepository.rewards;
  List<MissionAccomplished> get missions => _historicRepository.missions;

  Future<void> update() async {
    this.setState(ViewState.Busy);
    try {
      await _historicRepository.getHistoric();
      this.setState(ViewState.Idle);
    } catch (exception) {
      this.setErrorMessage(exception.toString());
      this.setState(ViewState.Error);
    }
  }
  
}